# Manejo de errores

* Crear una enumeración de errores llamada MovilError la cual contenga los valores:
   * sinBateria
   * creditoInsuficiente

* Crear una clase llamada TelefonoMovil que tenga.
   * Propiedad almacenada saldo (No puede ser negativo).
   * Propiedad almacenada carga de la bateria.
   * Propiedad almacenada Saldo consumido por minuto.
   * Propiedad almacenada Consumo de bateria por minuto.
   * Método hacerLlamada la cual puede fallar y toma un entero que representa la cantidad de minutos hablando. Esta debe imprimir algun tenxto por cada minuto que pasa hasta que la llamada finalice, la bateria muera o el saldo termine.
* Deben intentar hacerse llamadas, en caso de que se lance un error hay que evitar que este impira la ejecucuón del resto del programa.
