import Foundation

/*
# Manejo de errores

* Crear una enumeración de errores llamada MovilError la cual contenga los valores:
   * sinBateria
   * creditoInsuficiente

* Crear una clase llamada TelefonoMovil que tenga.
   * Propiedad almacenada saldo (No puede ser negativo).
   * Propiedad almacenada carga de la bateria.
   * Propiedad almacenada Saldo consumido por minuto.
   * Propiedad almacenada Consumo de bateria por minuto.
   * Método hacerLlamada la cual puede fallar y toma un entero que representa la cantidad de minutos hablando. Esta debe imprimir algun tenxto por cada minuto que pasa hasta que la llamada finalice, la bateria muera o el saldo termine.
* Deben intentar hacerse llamadas, en caso de que se lance un error hay que evitar que este impira la ejecucuón del resto del programa.
*/

enum MovilError: Error{
    case sinBateria
    case creditoInsuficiente
}

class TelefonoMovil{

    var saldo: Int
    var cargaDeBateria: Int

    static var saldoPorMinuto: Int = 1
    static var bateriaGastadaPorMinuto: Int = 2

    init(saldo: Int, bateria: Int){
        self.saldo = saldo < 0 ? 0 : saldo
        self.cargaDeBateria = bateria < 0 ? 0 : bateria
    }

    func hacerLlamada(minutos: UInt) throws -> Void{
        for _ in 1...minutos{
            if cargaDeBateria - TelefonoMovil.bateriaGastadaPorMinuto < 0{
                throw MovilError.sinBateria
            }
            if saldo - TelefonoMovil.saldoPorMinuto < 0{
                throw MovilError.creditoInsuficiente
            }
            saldo -= TelefonoMovil.saldoPorMinuto
            cargaDeBateria -= TelefonoMovil.bateriaGastadaPorMinuto
            print("Bla bla bla")
        }
    }
}


let teléfono: TelefonoMovil = TelefonoMovil(saldo: 13, bateria: 25)


do {
    try teléfono.hacerLlamada(minutos: 2)
    try teléfono.hacerLlamada(minutos: 20)
} catch MovilError.sinBateria {
    print("El teléfono no tiene batería")
} catch MovilError.creditoInsuficiente {
    print("El saldo es insuficiente, haga una recarga para continuar")
}
