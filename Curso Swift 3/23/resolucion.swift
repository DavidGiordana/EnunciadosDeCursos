import Foundation

/*
# Métodos

Crear una clase llamada ControlDeAcceso que contenga:
* Propiedades almacenadas de tipo (static) no modificables:
   * usuarioAdministrador: String
   * contraseñaAdministrador
* Propiedades almacenadas de instancia modificables:
   * usuario: String
   * ontraseña: String
* Métodos de instancia
   * iniciarSesion: (usuario: String, contraseña: String)->Void dada  la entrada debe imrpimir:
* Acceso Total, si el usuario es el administrador
* Bienvenido, si el usuario es un usuario normal
* Error: Información ingresada incorrecta, si los datos no corresponden a ningun usuario
*/

class ControlDeAcceso {

    static let usuarioAdministrador: String = "admin"
    static let contraseñaAdministrador: String = "1234"

    var usuario: String = "cliente123"
    var contraseña: String = "asdfgh"

    func iniciarSesion(usuario: String, contraseña: String){
        if ControlDeAcceso.usuarioAdministrador == usuario && ControlDeAcceso.contraseñaAdministrador == contraseña{
            print("Acceso Total")
        }
        else if self.usuario == usuario && self.contraseña == contraseña{
            print("Bienvenido")
        }
        else {
            print("Error: Información ingresada incorrecta")
        }
    }

}

let login = ControlDeAcceso()
login.iniciarSesion(usuario: "Juan", contraseña: "123")
login.iniciarSesion(usuario: "cliente123", contraseña: "asdfgh")
