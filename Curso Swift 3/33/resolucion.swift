import Foundation

/*
# Casteo de tipos

Utilizando las clases Animal, AnimalTerrestre y Perro crear una lista de objetos de tipo genérico. Reccorrer la lista y en cada iteración comprobar los tipos de los elementos y en caso de encontrar alguno conocido castearlo y obtener alguna descripción.
*/

class Animal {

    var nombre: String

    func obtenerDescripcion()->String{
        return "Animal: \(nombre)"
    }

    init(nombre: String){
        self.nombre = nombre
    }

    convenience init(){
        self.init(nombre: "Animal sin identificar")
    }

}


class AnimalTerrestre: Animal{

    var cantidadDePatas: Int

    override func obtenerDescripcion()->String{
        return "Animal Terrestre: \(self.nombre), tiene \(cantidadDePatas) patas"
    }

    init(nombre: String, cantidadDePatas: Int){
        self.cantidadDePatas = cantidadDePatas
        super.init(nombre: nombre)
    }

    override convenience init(nombre: String){
        self.init(nombre: nombre, cantidadDePatas: 4)
    }

}

class Perro: AnimalTerrestre{

    var raza: String?

    override func obtenerDescripcion()->String{
        var raza = ""
        if let r = self.raza{
            raza = " de raza \(r)"
        }
        return "Perro\(raza) tiene \(cantidadDePatas) patas"
    }

    init(raza: String?){
        self.raza = raza
        super.init(nombre: "perro", cantidadDePatas: 4)
    }

    convenience init (){
        self.init(raza: nil)
    }

}


let cosas: [Any] = [
        Perro(raza: "Dálmata"),
        17,
        AnimalTerrestre(nombre: "lemur"),
        Animal(nombre: "Gorrión")
]

for cosa in cosas{
    if cosa is Int{
        let i = cosa as! Int
        print(i)
    }
    else if cosa is Perro{
        let i = cosa as! Perro
        print(i.obtenerDescripcion())
    }
    else if cosa is AnimalTerrestre{
        let i = cosa as! AnimalTerrestre
        print(i.obtenerDescripcion())
    }
    else if cosa is Animal{
        let i = cosa as! Animal
        print(i.obtenerDescripcion())
    }
    print("")
}
