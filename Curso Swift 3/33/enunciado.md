# Casteo de tipos

Utilizando las clases Animal, AnimalTerrestre y Perro crear una lista de objetos de tipo genérico. Reccorrer la lista y en cada iteración comprobar los tipos de los elementos y en caso de encontrar alguno conocido castearlo y obtener alguna descripción.
