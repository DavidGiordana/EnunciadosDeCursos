import Foundation

/*
Tipos de datos básicos

* Almacenar el valor de pi (aproximadamente: 3.1416) en una constante de tipo Double y otra en una constante de tipo Float.
* Crear una constante llamada tres16 de tipo Uint16.
* Almacenar al valor de tres16 en una variable de tipo Int llamada tres.
* Guardar el resultado de restar a una de las constantes que contiene a pi el valor de la constante tres en una constante. Nota: Recordar las conversiones entre tipos.
* Convertir el valor obtenido en el paso anterior a String, agregarlo a una cadena que diga "La parte decimal de pi es " e imprimir el resultado
*/

let pi1: Double = 3.1416
let pi2: Float  = 3.1416

let tres16: Int16 = 3

var tres: Int = Int(tres16)

let piSinTres = pi1 - Double(tres)

let cadena: String = "La parte decimal de pi es " + String(piSinTres)
print(cadena)
