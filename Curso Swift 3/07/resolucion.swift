import Foundation

/*
# Strings PT 1

* Crear tres variables de tipo String llamadas cadena1, cadena2 y concatenada respectivamete. Estas deben contener los siguentes valoes:
   * cadena1: La palabra Hola
   * cadena2: La palabra Mundo precedida por un espacio
   * concatenada: El resultado de concatenar cadena1 y cadena2 empleando el operador de interpolación \()

* Almacenar el caracter punto en una constante con el mismo nombre (con letras). Agregar este caracter al final de concatenada

* Imprimir los caracteres de concatenda y la cantidad de caracteres que esta posee
*/

var cadena1: String = "Hola"
var cadena2: String = " Mundo"
var concatenada = "\(cadena1)\(cadena2)"
print(concatenada)

let punto: Character = "."
concatenada.append(punto)

print("\"\(concatenada)\" tiene \(concatenada.characters.count) caracteres")
