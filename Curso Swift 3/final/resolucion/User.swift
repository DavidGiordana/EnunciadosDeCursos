//
//  User.swift
//  filesystem
//
//  Created by davidgiordana on 4/1/17.
//  Copyright © 2017 David Giordana. All rights reserved.
//

import Foundation

// Representa un usuario del sistema
class User {

    // Nombre de usuario
    var name: String

    // Contraseña
    var password: String

    // Inicializador toma nombre y contraseña
    init(name: String, password: String) {
        self.name = name
        self.password = password
    }

}

// Extensión que permise usar un usario como clave de diccionario
extension User: Hashable {

    // Hash value vasado en el hash value del nombre de usuario
    var hashValue: Int {
        return name.hashValue
    }

    // Función de comparación
    static func ==(u1: User, u2: User)->Bool{
        return u1.name == u2.name
    }

}
