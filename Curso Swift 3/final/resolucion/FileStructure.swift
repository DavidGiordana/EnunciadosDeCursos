//
//  FileStructure.swift
//  filesystem
//
//  Created by davidgiordana on 4/1/17.
//  Copyright © 2017 David Giordana. All rights reserved.
//

import Foundation

// Representación abstracta de un archivo
class FileStructure {

    // Nombre del archivo
    var filename: String

    // Tamaño del archivo (default 0)
    var size: Int {
        return 0
    }

    // Índice de desplazamiento
    var currentIndex: Int = 0

    // Inicializa el archivo
    required init(filename: String){
        self.filename = filename
    }

    // Reinicia el índice del archivo (Lo pone en 0)
    func resetIndex() {
        currentIndex = 0
    }
}

// Protocolo con las operaciones que un archivo tendría que soportar
protocol FileOperationsProtocol {

    // Lee todo el contenido del archivo (no modifica el índice)
    func readAll() -> String

    // Lee una cantidad de caracteres del archivo
    func read(count: Int) -> String

    // Escribe un String en
    func write(data: String)

    // Elimina una cantidad de caracteres de un archivo
    // Se aplica hacia la derecha del índice actual
    func delete(count: Int)

    // Desplaza el índice
    func seek(count: Int)

}
