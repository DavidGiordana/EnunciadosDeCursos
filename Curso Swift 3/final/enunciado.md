# Trabajo final Curso del lenguaje de programación Swift 3 desde cero


## Introducción
Este proyecto tiene como objectivo crear un manejador de archivos simplificado que interactuará con múltiples usuarios dentro del equipo.

Para establecer controles se crearán usuarios en tiempo de ejecución y estos iniciarán sesión para hacer las operaciones.

Para simplificar el trabajo solo un usario podrá estar activo en ese momento. Aunque esto no quita la posibilidad de que otro usuario tenga abierto algún archivo en ese momento.

## La interacción con el usuario
El sistema interactuará con el usuario empleando comandos en modo texto. Toda solicitud ingresada debe ser procesada y retornar una respuesta con la información necesario o simplemente mostrar un error.

La entrada de información será procesadas por palabras. Tal y como ocurre en el español la separación entre las mismas es marcada por los espacios. Una abstracción permitida es no considerar ningún otro separador. Una excepción a esta regla serán los Strings literales los cuales tiene la siguiente forma:

*"Esto es un String literal"*

Como podemos ver dentro de este existen espacios. Este debe ser considerado una sola palabra (ignorando espacios). Los String literales deben escribirse entre comillas y al ser procesados la palabra debe ser su contenido (sin las comillas).

EJ: *"Hola Mundo"* = *Hola Mundo* (Como una sola palabra).

## Componentes del sistema
### CommLine
Debe contener todos los componentes de línea de comando que el usuario vea necesario. Entre ellos tenemos el parser. Un parser es una función que toma un String y lo convierte en una estructura útil para el programa.

Al ingresarse un texto este deberá ser parseado. Como ayuda dentro del parser se podría dividir La cadena en una lista de palabras (considerando el cuidado de los Strings literales) y con las mismas convertirlas en alguna estructura que contengan la información de un comando. A lo largo del curso se vieron diversas formas de hacerlo. Utilizar la que uno vea más conveniente y cómoda.

### Filesystem
Representa el sistema de archivos en su totalidad. Este debe recibir la entrada del usuario y aplicar las acciones que sean necesarias. Entre los elementos con los que debe contar tenemos una función que ejecute el ciclo principal del programa (lea y aplique cambios), una asociación entre nombre de usuario y objeto usuario, una asociación entre nombre de archivo y archivo, usuario actual.

Es posible que sea necesario incorporar más variables, esto queda a criterio del estudiante.

### FileStructureManager
Esta brinda una abstracción de un archivo y debe soportar todas las operaciones que este requiera. Entre estas tenemos:

1. Comprobar los permisos tanto del creador como del que abrió el archivo. Para modelizarlos utilizar un numero entero y con la técnica de la máscara (&amp;) hacer las comprobaciones. Este detalle debe permanecer oculto al resto del sistema.

2. Almacenar el creador y el usuario que abrió el archivo (si es que está abierto).

3. Contener la estructura de operaciones de un archivo.
Toda operación que el sistema de archivo requiera hacer debe ser con esta estructura.

### FileStructure
Representación abstracta de la que deben heredar las implementaciones de archivos. Debe tener:

* Nombre del archivo.
* Tamaño (en caracteres).
* Índice actual.
* Método para reiniciar el índice.

Esta abstracción permite a los hijos implementar la forma en la que la información de un archivo trabajará internamente. En la primer implementación se hará mediante una estructura de dato temporal que se destruirá la finalizar el programa. Se deja al alumno una nueva implementación que permita manejar archivos reales.

### FileOperationsProtocol
Protocolo con los elementos necesarios para manipular la información de un archivo. Este debe contener los métodos:

+ readAll() -> String

Lee todo el contenido del archivo (no modifica el índice).

+ read(count: Int) -> String

Lee una cantidad de caracteres del archivo.

+ write(data: String)

Escribe un String en el índice actual.

+ delete(count: Int)

Elimina el una cantidad de elementos a la derecha del índice actual hasta donde se le indique o lo permita el sistema.

+ seek(count: Int)

Desplaza el índice.

la estructura FileStructureManager debe hacer referencia a un elemento de tipo FileStructure que a su vez se adapte al protocolo FileOperationsProtocol. De esta manera se garantiza el correcto funcionamiento del sistema.

Todas las operaciones salvo readAll modifican el índice.
El índice es una forma de establecer un cursor en el archivo. Muchos sistemas de archivos o funciones de bajo nivel respetan esta forma de trabajar. Al leer parte del archivo el índice debe desplazarse. Este índice se inicializa cuando un archivo es abierto. Al cerrarse este debe reiniciarse y no será utilizado hasta que este se vuelva abrir. No tiene sentido leer en un archivo cerrado ¿verdad?

Una restricción que asegura la integridad de la información es solo permitir abrir y usar archivos a un usuario a la vez. Si otro quisiera usarlo al mismo tiempo se le debe denegar el acceso hasta que este sea cerrado. Además un archivo no debe poder ser abierto por alguien que no tiene permisos.

#### Los permisos
Existen tres tipos de permisos explícitos y uno implícito:
+ r (read): Permiso de lectura.
+ w (write): Permiso de escritura.
+ d (destruction): Permiso de destrucción.

Los dos primeros no requieren de mucha explicación. El tercero por su parte habilita a un usuario a borrar un archivo del sistema (mientras este lo tenga abierto o esté cerrado).

Estos permisos sirven para controlar la forma en que los otros usuarios pueden manipular el archivo. El propietario, por su parte, tiene todos los permisos implícitamente. De esta forma es el único que puede configurar los permisos y cambiar el propietario.

### User
Estructura de dato que contiene la información base como el nombre de usuario y contraseña (a priori con esto basta).

## Comandos disponibles
El sistema poseerá un conjunto de comandos útiles:

* create user &lt;Nombre>

Crea un usuario llamado Nombre si este no existe. En caso contrario se reportará error.

+ create file &lt;Nombre>

Crea un archivo siempre y cuando este no exista.

+ delete user &lt;Nombre>

Elimina un usuario siempre y cuando este exista y sea el que está usando la sesión actualmente.

+ delete file &lt;Nombre>

Elimina un archivo siempre y cuando este pertenezca al usuario actual o sea uno que tenga permisos de destruir y lo tenga abierto.

+ ls

Lista todos los archivos del sistema.

+ ls mine

Lista todos los archivos del sistema cuyo propietario es el usuario actual.

+ login &lt;Nombre>

Inicia sesión el usuario Nombre. Esta operación puede hacerse aunque otro usuario haya iniciado sesión.

+ logout

Cierra la sesión del usuario actual si es que hay uno. Em caso contrario falla.

+ change proprietary &lt;Archivo> &lt;Propietario>

Cambia el propietario del Archivo al usuario Propietario. Esto solo es permitido si el usuario actual es el propietario.

+ get permissions &lt;Archivo>

Muestra los permisos globales de un archivo.

+ set permissions &lt;Archivo> &lt;Permisos>

Configura los permisos de Archivo con las banderas Permiso.

+ read file &lt;Archivo> &lt;Cantidad>

Lee Cantidad caracteres de Archivo.

+ write file &lt;Archivo> &lt;Datos>

Escribe Datos en Archivo a partir del índice actual.

+ delete file &lt;Archivo> &lt;Cantidad>

Borra de Archivo Cantidad de caracteres hacia la derecha del índice.

+ seek file &lt;Archivo> &lt;Cantidad>

Desplaza el índice del Archivo Cantidad de veces. Positivo hacia la derecha y negativo hacia la izquierda. Debe compensar salidas de rango.

+ print file &lt;Archivo>

Imprime el contenido de Archivo.

+ info file &lt;Archivo>

Imprime la información de Archivo.

+ open file &lt;Archivo>

Abre un archivo si este está cerrado y se es propietario o se tiene algún permiso global.

+ close file &lt;Archivo>

Cierra el archivo si el que lo abrió es uno y este está abierto.

+ exit

Sale del intérprete.

Todas las operaciones salvo "create user", "login" y "exit" requieren estar en una sesión activa. En caso de no estarlo debe lanzarse un error.

Todo desplazamiento fuera del rango de un archivo debe ser corregido para impedir errores de consistencia.

### Manejo de errores
El sistema al detectar alguna condición que no se cumpla debe lanzar un error. Este no debe propagarse hasta el main sino que el ciclo de trabajo debe capturarlo e imprimir un mensaje de error y continuar con la espera del siguiente comando.

Para entender mejor los errores deben existir tres posibles grupos de errores:

+ Errores de ingreso: Representan algún problema al ingresar un comando, generalmente porque este no existe.

+ Errores de usuario; En ellos interviene el usuario como principal componente. EJ: El usuario no tiene permisos.

+ Errores de archivo: Son los más obvios y se relacionan, como su nombre lo indica, con los archivos. EJ: Archivo no existente.

## Un último detalle
A todos los que hicieron posible este curso y lo siguieron les agradezco mucho. Fue la primera vez que enseñé un lenguaje como tal y esta fue una experiencia valiosa para mi y espero el sentimiento sea mutuo. Muchas gracias a todos y ojalá lo hayan disfrutado.

Nos veremos en más videos, cursos y tutoriales así que no se desanimen que queda este último trabajo para concluir el curso. Espero ver sus implementaciones.

David Giordana 2017
