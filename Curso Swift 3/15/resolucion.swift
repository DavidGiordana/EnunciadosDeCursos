import Foundation

/*
# Condicional switch-case

* Crear un tipo que represente un color en RGB (recordar ejercicio de tuplas)
* Almacenar un color en una cosntante y utilizando un switch-case analizar lso casos:
   * Si r es 255: Imprimir "el color rojo está al maximo" junto y luego imprimir los valores restantes
   * Si g es 255: Imprimir "el color verde está al maximo" junto y luego imprimir los valores restantes
  * Si b es 255: Imprimir "el color azul está al maximo" junto y luego imprimir los valores restantes
* En caso contrario imprimir el valor RGB.
*/

typealias Color = (r: Int, g: Int, b: Int)

let colorActual: Color = (r: 10, g: 255, b: 90)

switch colorActual {
    case let (255, g, b):
        print("El color rojo está al máximo")
        print("El color verde tiene el valor \(g)")
        print("El color azul tiene el valor \(b)")
    case let (r, 255, b):
        print("El color verde está al máximo")
        print("El color rojo tiene el valor \(r)")
        print("El color azul tiene el valor \(b)")
    case let (r, g, 255):
        print("El color azul está al máximo")
        print("El color rojo tiene el valor \(r)")
        print("El color verde tiene el valor \(g)")
    case let (r, g, b):
        print("El color  actual es (R: \(r), G: \(g), B: \(b))")
}
