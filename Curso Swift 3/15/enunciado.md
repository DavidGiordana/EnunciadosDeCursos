# Condicional switch-case

* Crear un tipo que represente un color en RGB (recordar ejercicio de tuplas)
* Almacenar un color en una cosntante y utilizando un switch-case analizar lso casos:
   * Si r es 255: Imprimir "el color rojo está al maximo" junto y luego imprimir los valores restantes
   * Si g es 255: Imprimir "el color verde está al maximo" junto y luego imprimir los valores restantes
  * Si b es 255: Imprimir "el color azul está al maximo" junto y luego imprimir los valores restantes
* En caso contrario imprimir el valor RGB.
