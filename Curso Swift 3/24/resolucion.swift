import Foundation

/*
# Subscript

* Crear una implementación propia de diccionario en una clase. Para simplificar el trabajo el tipo de los datos será el equivalente a definir [Stirng: Int]. Esta implementación debe tener:
   * Una funcion limpiar que elimine todos los elementos del diccionario.
   * Dos propiedades computadas count e isEmpty (Su funcion se sobreentiende).
   * El agregado, acceso y modificacion de información se debe realizar mediante un subscript.
*/

class Diccinario {

    var datos: [(String, Int)] = [(String, Int)]()

    func limpiar(){
        self.datos = []
    }

    var count: Int {
        return datos.count
    }

    var isEmpty: Bool {
        return datos.isEmpty
    }

    subscript(llave: String) ->  Int? {
        get{
            var valor: Int?
            for (k,v) in datos{
                if (k == llave){
                    valor = v
                    break
                }
            }
            return valor
        }
        set{
            var agregado: Bool = false
            if !isEmpty {
                for i in 0..<count{
                    let (k,_): (String, Int) = datos[i]
                    if llave == k{
                        if let nv = newValue{
                            datos[i] = (k, nv)
                        }
                        else{
                            datos.remove(at: i)
                        }
                        agregado = true
                        break
                    }
                }
            }
            if !agregado, let nv = newValue{
                datos.append((llave, nv))
            }
        }
    }

}

let agenda: Diccinario = Diccinario()

print("El diccionario tiene \(agenda.count) elementos. Si revisamos isEmpty tenemos \(agenda.isEmpty)")
if let carlosNum = agenda["Carlos"]{
    print("El numero asociado a Carlos es \(carlosNum)")
}

agenda["Carlos"] = 123
print("El diccionario tiene \(agenda.count) elementos. Si revisamos isEmpty tenemos \(agenda.isEmpty)")
if let carlosNum = agenda["Carlos"]{
    print("El numero asociado a Carlos es \(carlosNum)")
}

agenda["Carlos"] = 456
print("El diccionario tiene \(agenda.count) elementos. Si revisamos isEmpty tenemos \(agenda.isEmpty)")
if let carlosNum = agenda["Carlos"]{
    print("El numero asociado a Carlos es \(carlosNum)")
}

agenda["Carlos"] = nil
print("El diccionario tiene \(agenda.count) elementos. Si revisamos isEmpty tenemos \(agenda.isEmpty)")
if let carlosNum = agenda["Carlos"]{
    print("El numero asociado a Carlos es \(carlosNum)")
}

/*
Este ejercicio tiene como objetivo entender el funcionamiento de los subscripts.
No es recomendable utilizar estas implementaciones en trabajos reales. Se podrá notar que 
la ejecución de este diccionario para grandes cantidades de datos es lenta, 
esto se debe a que los diccionarios implementados en la libraría estandar aprovechan otras estructuras
que son mejores que las listas en funcionamiento.
*/



