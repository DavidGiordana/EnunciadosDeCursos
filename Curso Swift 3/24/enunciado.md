# Subscript

* Crear una implementación propia de diccionario en una clase. Para simplificar el trabajo el tipo de los datos será el equivalente a definir [Stirng: Int]. Esta implementación debe tener:
   * Una funcion limpiar que elimine todos los elementos del diccionario.
   * Dos propiedades computadas count e isEmpty (Su funcion se sobreentiende).
   * El agregado, acceso y modificacion de información se debe realizar mediante un subscript.
