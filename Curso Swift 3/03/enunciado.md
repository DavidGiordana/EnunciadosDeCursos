# Tuplas y typealias

1. Crear un tipo (alias) llamado Color que tenga tras componentes donde sus nombres se corresponden con la siguiente lista
   * r será de tipo entero (Representa al color rojo)
   * g será de tipo entero (Representa al color verde)
   * b será de tipo entero (Representa al color azul)
   * El nombre de las componentes debe estar en el tipo.

2. Crear una varaible de tipo Color llamda colorActual que contenga el color negro (los tres campos en cero) y luego modificarlo para repreentar el color azul (todas las componentes en cero salvo b que vale 255)

3. Imprimir la segunda componente de la variabe creada previamente accediendola con la notación punto.

