# Control de acceso

Para esta ocasión el ejercicio es muy simple. En el ejercicio anterior creamos una implementación de Diccionario. Dentro de la misma el usuario puede acceder a datos de la implementación, es decir, puede saber qué estructuras utilizamos.
Para asegurar el cumplimento de la estructura de dato ocultar estos detalles usando control de acceso. Además se debe garantizar que la estructura pueda ser utulizada fuera del módulo.
