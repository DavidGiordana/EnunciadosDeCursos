import Foundation

/*
# Control de acceso

Para esta ocasión el ejercicio es muy simple. En el ejercicio anterior creamos una implementación de Diccionario. Dentro de la misma el usuario puede acceder a datos de la implementación, es decir, puede saber qué estructuras utilizamos.
Para asegurar el cumplimento de la estructura de dato ocultar estos detalles usando control de acceso. Además se debe garantizar que la estructura pueda ser utulizada fuera del módulo.
*/

public struct Diccionario<Clave: Equatable, Valor> {

    private var items = [(clave: Clave, valor: Valor)]()

    public subscript (clave: Clave)->Valor? {
        get {
            for (k,v) in items{
                if k == clave{
                    return v
                }
            }
            return nil
        }
        set(nuevoValor){
            for i in 0..<items.count {
                if items[i].clave == clave {
                    if let nv = nuevoValor {
                        items[i] = (clave, nv)
                    }
                    else{
                        items.remove(at: i)
                    }
                    return
                }
            }
            if let nv = nuevoValor {
                items.append((clave, nv))
            }
        }
    }

    public var isEmoty: Bool { return items.isEmpty }
    public var count: Int { return items.count }

    public var claves: [Clave] {
        return items.map {$0.0}
    }

}


func imprimirDiccionario(_ diccionario: Diccionario<Int, String>){
    for clave in diccionario.claves{
        print("\(clave): \(diccionario[clave]!)")
    }
}


var diccionario = Diccionario<Int,String>()
diccionario[435] = "Carlos"
diccionario[111] = "Ruben"
diccionario[5] = "Ana"
imprimirDiccionario(diccionario)
print("--------")
diccionario[111] = nil
imprimirDiccionario(diccionario)
