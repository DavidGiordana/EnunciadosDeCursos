# Protocolos PT 1

Crear un protocolo llamado Asociación con:
* Propiedad de solo lectura booleana llamada isEmpty (Indica si la estructura está vacía).

* Propiedad de solo lectura entera llamada count (Representa la cantidad de elemento de una estructura de datos).

* Método de instancia llamado agregar que toma un String y un entero opcional, y no retorna nada.

* Método de instancia llamado eliminar que toma un String y retorna un entero opcional.

* Método de instancia llamado existe que tome un String y retorne un booleano.

* Método de instancia llamado obtenerValor que tome un String y retorne un entero opcional.

Basándose en la implementación de Diccionario hecha en el capítulo de subscripts hacer que esta clase se adapte al protocolo Asociacion.
Para saber qué hace cada función solo es necesario leer su nombre y ver los tipos con los que trabaja,
Si lo ve necesario puede modificar el subscript para utilizar los métodos y propiedades definidos en el protocolo.
