import Foundation

/**
# Protocolos PT 1

Crear un protocolo llamado Asociación con:
* Propiedad de solo lectura booleana llamada isEmpty (Indica si la estructura está vacía).

* Propiedad de solo lectura entera llamada count (Representa la cantidad de elemento de una estructura de datos).

* Método de instancia llamado agregar que toma un String y un entero opcional, y no retorna nada.

* Método de instancia llamado eliminar que toma un String y retorna un entero opcional.

* Método de instancia llamado existe que tome un String y retorne un booleano.

* Método de instancia llamado obtenerValor que tome un String y retorne un entero opcional.

Basándose en la implementación de Diccionario hecha en el capítulo de subscripts hacer que esta clase se adapte al protocolo Asociacion.
Para saber qué hace cada función solo es necesario leer su nombre y ver los tipos con los que trabaja,
Si lo ve necesario puede modificar el subscript para utilizar los métodos y propiedades definidos en el protocolo.
**/

protocol Asociacion {

    var isEmpty: Bool {get}
    var count: Int {get}

    func agregar(clave: String, valor: Int?)

    func eliminar(clave: String)->Int?

    func existe(clave: String)->Bool

    func obtenerValor(clave: String)->Int?

}

class Diccinario: Asociacion {

    var datos: [(String, Int)] = [(String, Int)]()

    func limpiar(){
        self.datos = []
    }

    var count: Int {
        return datos.count
    }

    var isEmpty: Bool {
        return datos.isEmpty
    }

    func agregar(clave: String, valor: Int?){
        var agregado: Bool = false
        if !isEmpty {
            for i in 0..<count{
                let (k,_): (String, Int) = datos[i]
                if clave == k{
                    if let nv = valor{
                        datos[i] = (k, nv)
                    }
                    else{
                        datos.remove(at: i)
                    }
                    agregado = true
                    break
                }
            }
        }
        if !agregado, let nv = valor{
            datos.append((clave, nv))
        }
    }

    func eliminar(clave: String)->Int?{
        if !isEmpty {
            for i in 0..<count{
                let (k,v): (String, Int) = datos[i]
                if clave == k{
                    datos.remove(at: i)
                    return v
                }
            }
        }
        return nil
    }

    func existe(clave: String)->Bool{
        return obtenerValor(clave: clave) != nil
    }

    func obtenerValor(clave: String)->Int?{
        var valor: Int?
        for (k,v) in datos{
            if (k == clave){
                valor = v
                break
            }
        }
        return valor
    }


    subscript(llave: String) ->  Int? {
        get{
            return obtenerValor(clave: llave)
        }
        set{
            agregar(clave: llave, valor: newValue)
        }
    }

}


let agenda: Diccinario = Diccinario()

print("El diccionario tiene \(agenda.count) elementos. Si revisamos isEmpty tenemos \(agenda.isEmpty)")
if let carlosNum = agenda["Carlos"]{
    print("El numero asociado a Carlos es \(carlosNum)")
}

agenda["Carlos"] = 123
print("El diccionario tiene \(agenda.count) elementos. Si revisamos isEmpty tenemos \(agenda.isEmpty)")
if let carlosNum = agenda["Carlos"]{
    print("El numero asociado a Carlos es \(carlosNum)")
}

agenda["Carlos"] = 456
print("El diccionario tiene \(agenda.count) elementos. Si revisamos isEmpty tenemos \(agenda.isEmpty)")
if let carlosNum = agenda["Carlos"]{
    print("El numero asociado a Carlos es \(carlosNum)")
}

agenda["Carlos"] = nil
print("El diccionario tiene \(agenda.count) elementos. Si revisamos isEmpty tenemos \(agenda.isEmpty)")
if let carlosNum = agenda["Carlos"]{
    print("El numero asociado a Carlos es \(carlosNum)")
}
