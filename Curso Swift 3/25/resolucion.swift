import Foundation

/*
# Herencia

* Crear una clase principal llamada Animal. Esta representará al conjunto de todos los animales. Esta clase debe contar con una propiedad nombre y un método obtenerDescripión que a priori imprima el nombre del anumal  
* Crear una subclase de Animal llamada AnimalTerrestre la cual a su vez debe contar con la propiedad cantidad de patas.
* Heredar de la clase AnimalTerrestre con una clase llamada Perro la cual debe contener una propiedad raza que es un opcional.
* En cada caso hay que sovreescribir el método obtenerDescripción para que la salida tenga toda la información posible.
*/

class Animal {

    var nombre: String = ""

    func obtenerDescripcion()->String{
        return "Animal: \(nombre)"
    }

}


class AnimalTerrestre: Animal{

    var cantidadDePatas: Int = 4

    override func obtenerDescripcion()->String{
        return "Animal Terrestre: \(self.nombre), tiene \(cantidadDePatas) patas"
    }

}

class Perro: AnimalTerrestre{

    var raza: String?

    override func obtenerDescripcion()->String{
        var raza = ""
        if let r = self.raza{
            raza = " de raza \(r)"
        }
        return "Perro\(raza) tiene \(cantidadDePatas) patas"
    }

}

let perro: Perro = Perro()
perro.nombre = "perro"
perro.raza = "Dálmata"
print(perro.obtenerDescripcion())

let animalTerrestre: AnimalTerrestre = perro
print(animalTerrestre.obtenerDescripcion())

let animal: Animal = perro
print(animal.obtenerDescripcion())

//Aunque los tipos de animal y animal terrestre no son los mismos estamos
//almacenando a perro que es de tipo Perro y por lo tanto al llamar
//al método obtenerDescripcion tiene el comportamiento que tendriá
//para perro 
