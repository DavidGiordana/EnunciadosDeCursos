# Herencia

* Crear una clase principal llamada Animal. Esta representará al conjunto de todos los animales. Esta clase debe contar con una propiedad nombre y un método obtenerDescripión que a priori imprima el nombre del anumal  
* Crear una subclase de Animal llamada AnimalTerrestre la cual a su vez debe contar con la propiedad cantidad de patas.
* Heredar de la clase AnimalTerrestre con una clase llamada Perro la cual debe contener una propiedad raza que es un opcional.
* En cada caso hay que sovreescribir el método obtenerDescripción para que la salida tenga toda la información posible.
