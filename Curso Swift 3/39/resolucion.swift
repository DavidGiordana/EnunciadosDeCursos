import Foundation

/**
# Protocolos PT 5

* Crear un protocolo llamado ConNombre que tenga
    * propiedad de solo lectura de tipo String llamada nombre
* Crear un protocolo llamado Tamaño que tenga
    * propiedad de solo lectura de tipo Int llamada tamaño
* Crear una estructura que represente un archivo y lleve ese nombre. Esta a priori solo tendrá una propiedad llamada nombre.
* Extender Archivo para que se adapte a ConNombre y ConTamaño
* Definir una clase SistemaDeArchivos, esta debe tener:
    * Lista de archivos de tipo ConNombre y ConTamaño a la vez
    * Nombre
    * Inicializador correspondiente
    * Método para agregar archivo
    * Propiedad computada descripción que muestre el nombre del sistema de archivos y la lista de archivos con su nombre y tamaño. El criterio para calcular el tamaño es de libre eleción
* Hacer pruebas con todo lo definido
**/

// MARK: Protocolos

//Representa a los elementos con un nombre
protocol ConNombre {

    var nombre: String {get}

}

//Representa a los elementos con un tamaño
protocol ConTamaño {

    var tamaño: Int {get}

}

// MARK: Archivo

//Representación de arhcivo
//Omitiremos el contenido, no es necesario para el ejemplo
struct Archivo {

    var nombre: String

    init(nombre: String){
        self.nombre = nombre
    }

}

//Hacemos que la estructura archivo se conforma a los protocolos ConNombre y ConTamaño
extension Archivo: ConNombre, ConTamaño{

    internal var tamaño: Int {
        get {
            return self.nombre.characters.count
        }
    }

}

// MARK: Sistema de archivos

//Representación de un sistema de archivos
class SistemaDeArchivos {

    //Lista de archivos
    var archivos: [(ConNombre & ConTamaño)] = []

    //Nombre del sistema
    var nombre: String

    //Inicializador
    init(nombre: String){
        self.nombre = nombre
    }

    //Agrega un elemento a la lista de achivos si este no está registrado en caso contrario no haremos nada (decision del desarrollador)
    func agregar(archivo: (ConNombre & ConTamaño)){
        for item in archivos{
            if item.nombre == archivo.nombre {
                return
            }
        }
        self.archivos.append(archivo)
    }

    //Imprime una descripción del sistema
   var descripcion: String{
        var ret = "Sistema de archivos \"\(nombre)\""
        for archivo in archivos{
            ret += "\nArchivo \"\(archivo.nombre)\" de tamaño \(archivo.tamaño)"
        }
        return ret
    }

}

// MARK: prueba

var sistema = SistemaDeArchivos(nombre: "Sistema Principal")
sistema.agregar(archivo: Archivo(nombre: "Archivo 1"))
sistema.agregar(archivo: Archivo(nombre: "Archivo 2"))
sistema.agregar(archivo: Archivo(nombre: "Archivo 3"))
sistema.agregar(archivo: Archivo(nombre: "Archivo extra"))
print(sistema.descripcion)
