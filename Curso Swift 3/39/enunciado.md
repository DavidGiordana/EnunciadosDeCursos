# Protocolos PT 5

* Crear un protocolo llamado ConNombre que tenga
    * propiedad de solo lectura de tipo String llamada nombre
* Crear un protocolo llamado Tamaño que tenga
    * propiedad de solo lectura de tipo Int llamada tamaño
* Crear una estructura que represente un archivo y lleve ese nombre. Esta a priori solo tendrá una propiedad llamada nombre.
* Extender Archivo para que se adapte a ConNombre y ConTamaño
* Definir una clase SistemaDeArchivos, esta debe tener:
    * Lista de archivos de tipo ConNombre y ConTamaño a la vez
    * Nombre
    * Inicializador correspondiente
    * Método para agregar archivo
    * Propiedad computada descripción que muestre el nombre del sistema de archivos y la lista de archivos con su nombre y tamaño. El criterio para calcular el tamaño es de libre eleción
* Hacer pruebas con todo lo definido
