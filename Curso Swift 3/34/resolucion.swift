import Foundation

/*
Tipos anidados

Crear una clase biblioteca la cual contenga:
- Propiedad almacenada nombre: representará el nombre de la biblioteca
- Propiedad almacenada libros: Contendrá un grupo de objetos Libro
- Método para agregar un libro a la biblioteca
Dentro de la clase Biblioteca hay que definir un tipo Libro el cual a su vez contenga:
- Propiedad almacenada nombre
- Propiedad almacenada cantidadPaginas
- Propiedad almacenada autor
- Propiedad almacenada de tipo FormatoDeLibro
Dentro de la clase Libro crear una enumeración que tenga los elementos:
. digital
- impreso
*/

class Biblioteca {

    class Libro {

        enum FormatoDeLibro: String{
            case digital
            case impreso
        }

        var nombre: String
        var autor: String
        var formato: FormatoDeLibro
        var cantidadPaginas: Int

        init(nombre: String, autor: String, cantidadPaginas: Int, formato: FormatoDeLibro){
            self.nombre = nombre
            self.autor = autor
            self.formato = formato
            self.cantidadPaginas = cantidadPaginas
        }
    }

    var nombre: String

    var libros: [Libro]

    init(nombre: String){
        self.nombre = nombre
        self.libros = []
    }

    func agregar(libro: Libro){
        libros.append(libro)
    }

}

let bibliotecaMunicipal = Biblioteca(nombre: "Biblioteca Muniipal")
let libro1 = Biblioteca.Libro(nombre: "El grán código", autor: "Ruben", cantidadPaginas: 235, formato: .digital)
let libro2 = Biblioteca.Libro(nombre: "Las recetas irresistibles del 2017", autor: "Carlos", cantidadPaginas: 125, formato: .impreso)
bibliotecaMunicipal.agregar(libro: libro1)
bibliotecaMunicipal.agregar(libro: libro2)

for libro in bibliotecaMunicipal.libros{
    print("La biblioteca minicipal tiene el libro \"\(libro.nombre)\" escrito por el autor \(libro.autor). Este tiene \(libro.cantidadPaginas) páginas y está en formato \(libro.formato.rawValue).")
}
