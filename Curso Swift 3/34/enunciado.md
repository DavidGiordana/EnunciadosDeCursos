# Tipos anidados

1. Crear una clase biblioteca la cual contenga:
   * Propiedad almacenada nombre: representará el nombre de la biblioteca
   * Propiedad almacenada libros: Contendrá un grupo de objetos Libro
   * Método para agregar un libro a la biblioteca
2. Dentro de la clase Biblioteca hay que definir un tipo Libro el cual a su vez contenga:
   * Propiedad almacenada nombre
   * Propiedad almacenada cantidadPaginas
   * Propiedad almacenada autor
   * Propiedad almacenada de tipo FormatoDeLibro
3. Dentro de la clase Libro crear una enumeración que tenga los elementos:
   * digital
   * impreso
