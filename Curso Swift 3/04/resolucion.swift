import Foundation

/*
# Tipos de datos opcionales

* Crear una constante de tipo String con el texto de algún numero deseado
* Crear una variable de tipo Entero opcional donde se almacenará la conversion de la constante anterior a tipo Entero
* Imprimir el valor del elemento dentro del opcional con y sin unwrap. Para esto utilizar la estructura if oara extraer el valor de dentro de la variable y poder operarlo con seguiridad. En este paso también se reqerirá imprimir el valor del número. 
*/

let cadena: String = "123"

var numero: Int? = Int(cadena)

print(numero!)

if let numeroSeguro: Int = numero{
    print(numeroSeguro)
}
