# Protocolos PT 2

Tomando como referencia el protocolo GeneradorNumeroAleatorio y la clase LCG
hacer que GeneradorNumeroAleatorio tenga además:
* Un inicializador que tome una semilla de tipo Double
* Un método llamado randomRange que tome dos enteros y retorne otro
La idea es que en la implementación se utilice la semilla como primer valor para el cálculo pseudoaleatorio.
Con el método randomRange la intención es utilizar la funcion random para calcular un numero pseudoaleatorio entre
el primer argumento y el segundo incluidos.
Como LCG debe adaptarse al protocolo GeneradorNumeroAleatorio agregar los métodos que sean necesarios intentando
mantener la semántica antes descripta.
Crear una lista de al menos tres números de tipo Double. Utilizando un bucle for iterar sobre la misma y empleando
un generador que tome como semilla dicho valor genere varios numeros aleatorios.
Probar la función randomRange con al menos 10 generaciones.
