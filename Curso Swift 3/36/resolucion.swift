import Foundation

/**
# Protocolos PT 2

Tomando como referencia el protocolo GeneradorNumeroAleatorio y la clase LCG
hacer que GeneradorNumeroAleatorio tenga además:
* Un inicializador que tome una semilla de tipo Double
* Un método llamado randomRange que tome dos enteros y retorne otro
La idea es que en la implementación se utilice la semilla como primer valor para el cálculo pseudoaleatorio.
Con el método randomRange la intención es utilizar la funcion random para calcular un numero pseudoaleatorio entre
el primer argumento y el segundo incluidos.
Como LCG debe adaptarse al protocolo GeneradorNumeroAleatorio agregar los métodos que sean necesarios intentando
mantener la semántica antes descripta.
Crear una lista de al menos tres números de tipo Double. Utilizando un bucle for iterar sobre la misma y empleando
un generador que tome como semilla dicho valor genere varios numeros aleatorios.
Probar la función randomRange con al menos 10 generaciones.
**/

protocol GeneradorNumeroAleatorio{

    init(semilla: Double)

    func random()->Double

    func randomRange(_ a: Int, _ b: Int)->Int

}

class LCG: GeneradorNumeroAleatorio{

    var ultimoValor: Double
    let m = 139968.0
    let a = 3877.0
    let c = 23573.0

    required init(semilla: Double){
        self.ultimoValor = semilla
    }

    func random() -> Double {
        ultimoValor = (ultimoValor * a + c).truncatingRemainder(dividingBy: m)
        return ultimoValor / m
    }

    func randomRange(_ a: Int, _ b: Int)->Int{
        return Int(random() * Double(b)) + a
    }

}


let semillas = [42.0, 33.5, 5, 0.035]
for sem in semillas{
    let generador: GeneradorNumeroAleatorio = LCG(semilla: sem)
    var resultados = "Utulizando la semilla \(sem) obtenemos "
    for _ in 1...5{
        resultados += "\(generador.random())  "
    }
    print(resultados)
}

print("")

let generador: GeneradorNumeroAleatorio = LCG(semilla: 42.0)
var resultados = ""
for _ in 1...30{
    resultados += "\(generador.randomRange(1,6)) "
}
print(resultados)
