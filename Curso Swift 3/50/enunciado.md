# Operadores Avanzados PT 2

Crear la estructura de dato Punto2D. Esta cuenta con dos valores Double llamados x e y los cuales representan su posición en un plano bidimensional.
Incorporar los siguentes operadores mediante una extensión
* + -> operador infijo que realiza la suma de dos puntos. Esto es componente a componente. EJ (x: 1, y: 1) + (x: 3, y: 3) = (x: 4, y: 4)
* - -> operador infijo que realiza la resta de dos puntos. Esto es componente a componente. EJ (x: 1, y: 1) - (x: 3, y: 3) = (x: -2, y: -2)
* * -> operador infijo que realiza la multiplicación de un escalar por un punto. Esto es se multiplica a cada componente el escalar. EJ 3 * (x: 1, y: 1) = (x: 3, y: 3)
* += -> suma compuesta
* -= -> resta compuesta
* == y != -> operadores de comparación. Dos puntos son iguales si sus componentes son iguales.
* <> -> Operador infijo que toma un punto y un entero entre 1 y 4 y retorna otro punto. La precedencia del mismo es equivalente al de la suma. La función del operador es hacer una proyección en algún cuadrante de un plano 2D: El cuadrante 1 comprende los valores de x e y positivos, el 2 x negativos e y positivos, 3 x e y negativos, y el 4 x positivos e y negativos. EJ: (x:2, y:3) <> 3 = (x: -2, y:-3).

Pista: La función abs(_:) puede ser útil, esta calcula el valor absoluto de un número.
