# Ejercicios: operadores avanzados PT 1

Definir las siguientes funciones
.. esImpar que tome un Int8 e indique si estte es impar con operaciones de bits.
Ayuda: Los numeros impares tiene el dígito de la derecha en 1
.. contarUnos que tone un int8 y cuente la cantidad de dígitos 1 que este contiene
.. esPositivo: dado un Int8 indica si el número es positivo o negativo
.. imprimirBinario que tome un Int8 e imprima el número en binario. Esta función es similar String(\_:radix:) pero no tiene problema con los números negativos

Para hacer esto más completo se pueden agregar las funciones al tipo con una extensión e implementar las versiones para Int16, Int32 e Int64
