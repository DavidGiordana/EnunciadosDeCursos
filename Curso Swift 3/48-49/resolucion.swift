import Foundation

/*
# Ejercicios: operadores avanzados PT 1

Definir las siguientes funciones
.. esImpar que tome un Int8 e indique si estte es impar con operaciones de bits.
Ayuda: Los numeros impares tiene el dígito de la derecha en 1
.. contarUnos que tone un int8 y cuente la cantidad de dígitos 1 que este contiene
.. esPositivo: dado un Int8 indica si el número es positivo o negativo
.. imprimirBinario que tome un Int8 e imprima el número en binario. Esta función es similar String(_:radix:) pero no tiene problema con los números negativos

Para hacer esto más completo se pueden agregar las funciones al tipo con una extensión e implementar las versiones para Int16, Int32 e Int64
*/


func esImpar(_ numero: Int8) -> Bool{
	return (numero & 0b00000001) != 0
}

func contarUnos(_ numero: Int8)->Int {
	var temp = numero
	var resultado = 0
	for _ in 1...8 {
		if esImpar(temp) {
			resultado += 1
		}
		temp = temp >> 1
	}
	return resultado
}

func esPositivo(_ numero: Int8)->Bool{
	let mascara: Int8 = 0b1000000
	return numero & mascara != mascara
}

func imprimirBinario(_ n: Int8) {
	let mascara: Int8 = 0b1 // Es lo mismo que 1
	var temp = n
	var cadena = ""
	for _ in 1...8 {
		cadena = "\(temp & mascara)" + cadena
		temp >>= 1
	}
	print(cadena)
}

let numero1: UInt8 = 17
let numero2: Int8 = -17
let n1i = esImpar(Int8(numero1))
let n2i = esImpar(numero2)
let countN1 = contarUnos(Int8(numero1))
let countN2 = contarUnos(numero2)
let positivoN1 = esPositivo(Int8(numero1))
let positivoN2 = esPositivo(numero2)

print("El número \(numero1) es par? \( n1i ? "no" : "si")")
print("El número \(numero1) es par? \( n2i ? "no" : "si")")
print("El número \(numero1) tiene \(countN1) numeros 1")
print("El número \(numero2) tiene \(countN2) numeros 1")
print("El número \(numero1) es positivo? \( positivoN1 ? "si" : "no")")
print("El número \(numero2) es positivo? \( positivoN2 ? "si" : "no")")
print("El número \(numero1) se escribe: ", terminator: "")
imprimirBinario(Int8(numero1))
print("El número \(numero2) se escribe: ", terminator: "")
imprimirBinario(Int8(numero2))
