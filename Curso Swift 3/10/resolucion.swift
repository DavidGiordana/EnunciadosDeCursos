import Foundation

/*
#Conjuntos

* Crear dos conjuntos de enteros vacíos. Al primero se le deberá insertar los valores 1, 2 y 3 y al segundo habrá que asignarle el arreglo literal [3, 4, 3, 5].

* Aplicar operacines de conjuntos entre ambos conjuntos (Deben ser al menos cuatro).

* Imprimir los elementos ordenados de un nuevo conjunto de enteros que debe contener los valores 4, 6, 7, 1 y 0.
*/

var conjunto1: Set<Int> = Set<Int>()
var conjunto2: Set<Int> = []

print(conjunto1) ; print(conjunto2); print("")

conjunto1.insert(1)
conjunto1.insert(2)
conjunto1.insert(3)

conjunto2 = [3, 4, 3, 5]

print(conjunto1) ; print(conjunto2); print("")

print("La union entre \(conjunto1) y \(conjunto2) da como resultado \(conjunto1.union(conjunto2))")
print("La intersección entre \(conjunto1) y \(conjunto2) da como resultado \(conjunto1.intersection(conjunto2))")
print("La diferencia entre \(conjunto1) y \(conjunto2) da como resultado \(conjunto1.subtract(conjunto2))")
print("La diferencia simétrica entre \(conjunto1) y \(conjunto2) da como resultado \(conjunto1.symmetricDifference(conjunto2))")

print("")

var nuevoConjunto: Set<Int> = [4, 6, 7, 1, 0]
for elemento in nuevoConjunto.sorted(){
    print(elemento)
}
