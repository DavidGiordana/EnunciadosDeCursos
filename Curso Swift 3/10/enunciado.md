#Conjuntos

* Crear dos conjuntos de enteros vacíos. Al primero se le deberá insertar los valores 1, 2 y 3 y al segundo habrá que asignarle el arreglo literal [3, 4, 3, 5].

* Aplicar operacines de conjuntos entre ambos conjuntos (Deben ser al menos cuatro).

* Imprimir los elementos ordenados de un nuevo conjunto de enteros que debe contener los valores 4, 6, 7, 1 y 0.
