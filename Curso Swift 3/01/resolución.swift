import Foundation

/*
Variables, Constantes y Comentarios

Crear una variable de tipo String llamada lenguaje que contenga la palabra swift.
Crear una constante de tipo entero llamada centena que almacene el numero 100.
Imprimir el contenido de la variable y constante definidas anteriormente tanto en una sola linea como en dos lineas separadas.
*/

// |
var lenguaje: String = "swift"

// 2
let centena: Int = 100

//3 a
print(lenguaje); print(centena)

//3 b
print(lenguaje)
print(centena)
