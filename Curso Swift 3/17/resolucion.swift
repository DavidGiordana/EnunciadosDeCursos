import Foundation

/*
# Funciones PT 1

Crear una funcion división que tome dos valores enteros y retorne una tupla (cociente, resto). Esta debe realizar la operación valor1 / valor2 siendo "/" el operador de division entera. Para hacerlo no se puede emplear el operador de división. PISTA: Para dividir utilzar la resta.
*/

func division(_ dividendo: Int, _ divisor: Int)->(cociente: Int, resto: Int){
    var cociente: Int = 0
    var resto: Int = dividendo
    while resto >= divisor{
        cociente += 1
        resto -= divisor
    }
    return (cociente, resto)
}

let valor1: Int = 11
let valor2: Int = 2
let (cociente, resto) = division(valor1, valor2)
print("El resultado de dividir \(valor1) y \(valor2) da \(cociente) y tiene un resto de \(resto)")
