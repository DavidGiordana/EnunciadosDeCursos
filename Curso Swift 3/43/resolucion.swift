import Foundation

/*
# Genéricos PT 1

Crear una estructura Cola que tenga un tipo genérico que tenga:
* método encolar que tome un elemento del tipo asociado a la estructura y lo agregue a la Cola
*  método desencolar que retorne un elemento del tipo asociado a la estructura opcional. (nil si no se puede desencolar)
Las colas respetan el paradigma FIFO (First In First Out) o Primero Entra, Primero Sale. Esto quiere decir que los elementos se encolan y al desencolarse salen en el mismo orden que fueron ingresados. Para darse una mejor idea pensar en una cola en cualquier comercio o banco, el primero en llegar es atendido antes que los demás.
Extender la estructura para incorporar las propiedades count e isEmpty.
*/
struct Cola<Elemento> {

    var items = [Elemento]()

    mutating func encolar(_ item: Elemento){
        self.items.append(item)
    }

    mutating func desencolar()->Elemento?{
        if items.isEmpty {
            return nil
        }
        return self.items.removeFirst()
    }

}

extension Cola {

    var count: Int { return self.items.count }

    var isEmpty: Bool { return self.items.isEmpty }

}

var cola = Cola<Int>()
cola.encolar(1)
cola.encolar(2)
cola.encolar(3)
cola.encolar(4)
print("La cola tiene \(cola.count) elementos")
while !cola.isEmpty {
    print(cola.desencolar()!)
}
