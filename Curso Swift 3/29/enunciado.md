# Conteo automático de referencias

* Crear una clase Banco que tenga:
   * Propiedad almacenada nombre
   * Lista de objetos Persona (al trabajar con esta debemos asegurarnos de no permitir duplicados)
   * Método registarCliente que tome una Persona, la agregue al conuunto e imprima que esta se registró
   * Método darDeBaja que tome un objeto Persona e intente eliminarlo

* Crear una clase Persona que tenga:
   * Propiedad almacenada nombre
   * Propiedad almacenada opcional banco

* Crear una instancia de cada clase en propiedades opcionales, registar al cliente en el banco y destruir al banco (supondremos que este cerró por fraude)
* Para comprobar que los elementos se destruyen cunado corresponde agregar desinicializadores que impriman al realizarse dicha acción.
* Esta implementacion probablemente genere un ciclo de referencias fuertes. Solucione ese probleme empleando lo visto en el video.
