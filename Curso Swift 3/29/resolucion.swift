import Foundation

/*
# Conteo automático de referencias

* Crear una clase Banco que tenga:
   * Propiedad almacenada nombre
   * Lista de objetos Persona (al trabajar con esta debemos asegurarnos de no permitir duplicados)
   * Método registarCliente que tome una Persona, la agregue al conuunto e imprima que esta se registró
   * Método darDeBaja que tome un objeto Persona e intente eliminarlo

* Crear una clase Persona que tenga:
   * Propiedad almacenada nombre
   * Propiedad almacenada opcional banco

* Crear una instancia de cada clase en propiedades opcionales, registar al cliente en el banco y destruir al banco (supondremos que este cerró por fraude)
* Para comprobar que los elementos se destruyen cunado corresponde agregar desinicializadores que impriman al realizarse dicha acción.
* Esta implementacion probablemente genere un ciclo de referencias fuertes. Solucione ese probleme empleando lo visto en el video.
*/

class Banco {

    var nombre: String

    var clientes: [Persona]

    init(nombre: String){
        self.nombre = nombre
        self.clientes = []
    }

    func registar(cliente: Persona){
        var registrado = false
        for cli in clientes{
            if cli === cliente{
                registrado = false
                break
            }
        }
        if !registrado{
            cliente.banco = self
            clientes.append(cliente)
        }
    }

    func darDeBaja(cliente: Persona){
        if clientes.isEmpty{
            return
        }
        for i in 0..<clientes.count{
            if clientes[i] === cliente{
                clientes[i].banco = nil
                clientes.remove(at: i)
                break
            }
        }
    }

    deinit {
      print("Se está destruyendo el banco \(nombre)")
    }

}

class Persona {

    var nombre: String

    weak var banco: Banco?

    init(nombre: String, banco: Banco? = nil){
        self.nombre = nombre
        self.banco = banco
    }

    deinit{
        print("Se está destruyendo la persona \(nombre)")
    }

}

var bancoEstatal: Banco? = Banco(nombre: "Banco del estado")
var persona1: Persona? = Persona(nombre: "Carlos")
if let banco = bancoEstatal, let persona = persona1{
    banco.registar(cliente: persona)
}
bancoEstatal = nil
persona1 = nil
