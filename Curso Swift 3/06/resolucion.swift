import Foundation

/*
# Operadores básicos PT 2

* Crear dos constante de tipo booleano llamadas valorb1 y valorb2. Abas deberán contener algún valor booleano que el estudiante desee.
* Imprimir el resultado de emplear los operadores lógicos sobre las constantes creadas anteriormetne. Las operaciones a realizar deben ser and (y), or (o) e igual.

* Crear dos cosntantes de tipo entera. La primera se sebe llamar valor y contener algún numero (puede ser negativo). La segunda por su parte recibirá el nombre de valorAbsoluto y debe ser rellenada emplenado el operador de comparación ternario. La condición debe evaluar si la constante valor es mayor o igual a cero. Si se dá el caso se debe devolver el mismo numero y en caso contrario hay que retornar el opuesto. Finalmente imprimir el valor y valor absoluto. este ultimo ejercicio no es nada más ni nada menos que el funcionamiento del valor absoluto matemático.

* Crear una constante llamada cadena de tipo String que tenga algún string (en lo posuble un número). Crear una constante llamada numero de tipo entero que almacene el resultado de convertir cadena en un entero. Esta operación retorna un tipo opcional. Empleando el nuevo operador introducido hacer que si el resultado es nil (no es un número) se guarde el valor por defecto cero.
*/

let valorb1: Bool = false
let valorb2: Bool = true

print(valorb1 && valorb2)
print(valorb1 || valorb2)
print(valorb1 == valorb2)


let valor: Int = 17
let valorAbsoluto: Int = valor <= 0 ? -valor : valor
print(valorAbsoluto)


let cadena: String = "123"
let numero = Int(cadena) ?? 0
print(numero)

/*
Si se está interesado en entender mejor los operadores booleanos se recomienda leer sobre lógica proposicional. Existen muchas propiedades que pueden simplificar las fórmulas y brinda un pensamiento mas estructurado
*/
