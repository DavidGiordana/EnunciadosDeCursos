# Clausuras

* Crear una clausura que tome como parámetro un entero y retorne un booleano. Esta debe evaluar si el numero pasado es impar o no (Un numero es impar si al dividirlo por dos el resto es 1). Esta clausura debe almacenarse en una cosntante.
* Crear una clausura que tome dos enteros y retorne un booleano. La misma debe retornar true solo si el segundo valor es menor que el primero. Esta clausura al igual que la anterior debe almacenarse en una constatante.

* Crear una lista de valores enteros que tenga una longitud mínima de cinco.
Generar una lista que contenga elementos que satisfagan la condición de la primer clausura y estén ordenados bajo el criterio de la segunda. Imprimir el resultado.
* NOTA: La operación de filtrado se realiza mediante la funcion filter de listas. La sintaxis para usarla es la siguiente lista.filter clausura, donde clausura toma como parámetro un elemento del tipo de la lista y retorna un booleano.
