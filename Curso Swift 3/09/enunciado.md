# Arreglos

* Crear tres arreglos de String vacios utilizando las tres notaciones vistas.
* A cada arreglo agregar los siguentes elementos:
   * primero: "Hola" "mundo"
   * segundo "sean" "bienvenidos"
   * tercero "al" "curso" "de" "swift"
* Crear un arreglo literal (describir los elementos entre corchetes) de tipo entero que contenga los numeros del 1 al 10
* Empleando el operador ternario imprimir "El arreglo tiene n elementos" si el arreglo no está vacío y en caso contrario imprimir "Este arreglo está vacío"
* Al ultimo arreglo eliminarle el primer elemento y el último, luego de lo cual se debe imprimir cada elemento utilizando un for.
