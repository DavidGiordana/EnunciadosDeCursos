import Foundation

/*
# Arreglos

* Crear tres arreglos de String vacios utilizando las tres notaciones vistas.
* A cada arreglo agregar los siguentes elementos:
   * primero: "Hola" "mundo"
   * segundo "sean" "bienvenidos"
   * tercero "al" "curso" "de" "swift"
* Crear un arreglo literal (describir los elementos entre corchetes) de tipo entero que contenga los numeros del 1 al 10
* Empleando el operador ternario imprimir "El arreglo tiene n elementos" si el arreglo no está vacío y en caso contrario imprimir "Este arreglo está vacío"
* Al ultimo arreglo eliminarle el primer elemento y el último, luego de lo cual se debe imprimir cada elemento utilizando un for.
**/

var arreglo1: [String] = Array<String>()
var arreglo3: [String] = [String]()
var arreglo2: [String] = []
print(arreglo1); print(arreglo2); print(arreglo3); print("")

arreglo1.append("Hola")
arreglo1.append("mundo")

arreglo2.append("sean")
arreglo2.append("bienvenidos")

//Otra forma de agregar elementos a un arreglo
//(en este caso porque está vacio y no nos importa lo anterior)
//es hacer una asignación directa
arreglo3 = ["al", "curso", "de", "swift"]

print(arreglo1); print(arreglo2); print(arreglo3); print("")

var numeros: [Int] = [1,2,3,4,5,6,7,8,9,10]

let respesta: String = numeros.isEmpty ? "Este arreglo está vacío" : "El arreglo tiene \(numeros.count) elementos"
print(respesta)

numeros.remove(at: 0)
numeros.remove(at: numeros.count - 1)
for elemento in numeros{
    print(elemento)
}
