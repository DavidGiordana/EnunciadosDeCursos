import Foundation

/*
# Inicializadores PT 2

Tomando el ejercicio visto en el capítulo de herencia de clases agregarle inicializadores designados de manera tal que no existan valores por defecto que uno pueda suponer. Además incorporar inicializadores por conveniencia donde vea que puede ayudar.
*/

class Animal {

    var nombre: String

    func obtenerDescripcion()->String{
        return "Animal: \(nombre)"
    }

    init(nombre: String){
        self.nombre = nombre
    }

    convenience init(){
        self.init(nombre: "Animal sin identificar")
    }

}


class AnimalTerrestre: Animal{

    var cantidadDePatas: Int

    override func obtenerDescripcion()->String{
        return "Animal Terrestre: \(self.nombre), tiene \(cantidadDePatas) patas"
    }

    init(nombre: String, cantidadDePatas: Int){
        self.cantidadDePatas = cantidadDePatas
        super.init(nombre: nombre)
    }

    override convenience init(nombre: String){
        self.init(nombre: nombre, cantidadDePatas: 4)
    }

}

class Perro: AnimalTerrestre{

    var raza: String?

    override func obtenerDescripcion()->String{
        var raza = ""
        if let r = self.raza{
            raza = " de raza \(r)"
        }
        return "Perro\(raza) tiene \(cantidadDePatas) patas"
    }

    init(raza: String?){
        self.raza = raza
        super.init(nombre: "perro", cantidadDePatas: 4)
    }

    convenience init (){
        self.init(raza: nil)
    }

}

let perro: Perro = Perro()
perro.raza = "Dálmata"
print(perro.obtenerDescripcion())
