import Foundation

/*
# Declaraciones de transferencia de control

* Crear una lista de Strings con palabras sueltas. La única condicion para trabajar es que alguna de las palabras ingresadas en el arreglo debe ser FIN. Esta palabra clave se utilizará como marcador de fin de lectura.
* Empleando un bucle for recorrer el arreglo e imprimir el texto resultante de ombinar las palabras. Para que el ejercicio sea más interesante las palabras no tienen que estar separadas por un salto de linea sino que debe leerse como un texto normal donde los separadores son esoacios, es decir, la lista ["Hola", "mundo"] debe imprimir Hola mundo.
* Como agregado al ejercicio anterior considerar la palabra clave NUEVA que haga un salto de linea.
* Si en algún momento de la lectura el programa se topa con una palabra que comience con FIN debe interrumpirse la lectura
*/

//Version 1
var palabras: [String] = ["Este", "es", "un", "texto", "para", "imprimir", "FIN",
                            "Esta", "es", "otra", "oración", "FIN"]
var temporal: String = ""
for palabra in palabras{
    if palabra == "FIN" {
        print(temporal)
        break
    }
    temporal += " " + palabra
}

//prepara las cosas de la segunda version
temporal = ""
palabras = ["Este", "es", "un", "texto", "para", "imprimir", "NUEVA",
                            "Esta", "es", "otra", "oración", "FIN", "para", "procesar"]
print("")

//version extendida
for palabra in palabras{
        if palabra == "NUEVA"{
            print(temporal)
            temporal = ""
            continue
        }
        else if palabra == "FIN" {
            print(temporal)
            break
        }
        temporal += " " + palabra
}
