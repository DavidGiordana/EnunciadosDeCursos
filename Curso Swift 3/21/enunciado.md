# Clases y estructuras

* Con anterioridad definimos un tipo con tuplas la representacion de colores en RGB. En lugar de emplear tuplas que no permiten contener funciones ni valores extras es mejor utilizar otros medios. Para este ejercicio representar colores empleando una estructura.
* Crear una clase llamada RGBA que contenga una propiedad (variable) llamada color de tipo Color (el tipo definido en el ejercicio anterior) y otra propiedad de tipo Double llamada opacidad. Esta última se utilizará para representar las transparencias en nuestro modelo- Los valores que puede abarcar van del 0 al 1 incluidos

* Crear una instancia de color rojo transparente (r:255  g:0  b:0  opacidad:0).
* Crear una instancia de color verde semitransparente (r:0  g:255  b:0  opacidad:0.5).
* Crear una instancia de color azul opaco (r:0  g:0  b:255  opacidad:1).
