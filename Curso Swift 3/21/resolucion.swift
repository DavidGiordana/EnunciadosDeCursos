import Foundation

/*
# Clases y estructuras

* Con anterioridad definimos un tipo con tuplas la representacion de colores en RGB. En lugar de emplear tuplas que no permiten contener funciones ni valores extras es mejor utilizar otros medios. Para este ejercicio representar colores empleando una estructura.
* Crear una clase llamada RGBA que contenga una propiedad (variable) llamada color de tipo Color (el tipo definido en el ejercicio anterior) y otra propiedad de tipo Double llamada opacidad. Esta última se utilizará para representar las transparencias en nuestro modelo- Los valores que puede abarcar van del 0 al 1 incluidos

* Crear una instancia de color rojo transparente (r:255  g:0  b:0  opacidad:0).
* Crear una instancia de color verde semitransparente (r:0  g:255  b:0  opacidad:0.5).
* Crear una instancia de color azul opaco (r:0  g:0  b:255  opacidad:1).
*/

struct RGB {
    var r: Int = 0
    var g: Int = 0
    var b: Int = 0
}

class RGBA {
    var color: RGB = RGB()
    var opacidad: Double = 1
}

var rojo: RGBA = RGBA()
rojo.color.r = 255
rojo.opacidad = 0
print("Tenemos el color rojo (r: \(rojo.color.r), g:\(rojo.color.g), b:\(rojo.color.b)) con opaidad \(rojo.opacidad)")

var verde: RGBA = RGBA()
verde.color.g = 255
verde.opacidad = 0.5
print("Tenemos el color verde (r: \(verde.color.r), g:\(verde.color.g), b:\(verde.color.b)) con opaidad \(verde.opacidad)")

var azul: RGBA = RGBA()
azul.color.b = 255
print("Tenemos el color azul (r: \(azul.color.r), g:\(azul.color.g), b:\(azul.color.b)) con opaidad \(azul.opacidad)")
