import Foundation

/*
# Diccionarios 

* Crear un diccionario llamado agenda que se utilizará como agenda de contactos. En esta se hará la asociación de nombres a numeros. Supondremos que cada contacto puede tener un único numero. La agenda debe tener los siguentes contactos al comenzar:
   * Carlos -> 231
   * Ruben -> 555
   * Maria -> 952
* Agregar el contacto Julieta con el numero 635.
* Imprimir el contenido de la agenda ordenados.
* Imprimir el numero de Carlos. El resultado no debe ser opcional.
* Eliminar a carlos de la lista de contactos.
* Actualizar el numero de Ruben a 908.
* Imprimir la lista de contactos ordenados alfabeticamente.
*/


var agenda: [String: Int] = ["Carlos": 231,
                             "Ruben": 555,
                             "Maria": 952]

print(agenda)

agenda["Julieta"] = 635

print(agenda); print("")

if let numeroDeCarlos = agenda["Carlos"]{
    print("El número de carlos es \(numeroDeCarlos)")
}

print("")

agenda["Carlos"] = nil

print(agenda); print("")

agenda["Ruben"] = 908

print(agenda); print("")

for nombre in agenda.keys.sorted(){
    if let numero = agenda[nombre]{
        print("\(nombre): \(numero)")
    }
}
