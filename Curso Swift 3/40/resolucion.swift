import Foundation


/*
# Protocolos PT 6

* Crear un protocolo Proceso que tenga:
    * Propiedad nombre.
    * Método opcional llamado mostrar que no tome nada y retorne un String.
    * Método llamado procesar que no tome nada y retorne un String.
* Definir una clase Procesador que tenga:
    * Lista de procesos.
    * Nombre.
    * Inicializador.
    * Método para agregar procesos.
    * Método porcesar.
Este último debe mostar información del procesador y respetar la siguente estructura de ejecucióm
* LLamar a la función procesar de un proceso. Si este retorna nil significa que su ejecución fue satisfactoria. En caso contrario hubo algún problema (no es una excepción necesariamente, es una forma de representarlo) y el String resultante es una descripción del error.
* Si el paso anterior no falló mostrar el mensaje de salida del programa en pantalla en caso de existir alguno.
El funcionamiento de la lista debe emular a una cola, es decir, que el primero que entró será el primero atendido y será eliminado de la lista. Al finalizar la ejecución del método procesar la lista de procesos debe quedar vacía.

* Crear procesos propios, al menos 3 y hacer pruebas con el procesador.
 */


// MARK: Protocolo Proceso

@objc protocol Proceso {

    var nombre: String {get}

    @objc optional func mostrar()->String

    @objc func procesar()->String?

}

// MARK: Administrador de procesos

class Procesador {

    //Lista de procesos
    var procesos = [Proceso]()

    //Nombre del administrador
    var nombre: String

    //Inicializador
    init(nombre: String){
        self.nombre = nombre
    }

    //Agrega un proceso a la lista
    func agregar(proceso: Proceso){
        self.procesos.append(proceso)
    }

    //Comienza a processar, su funcionamiento se basa en una cola de procesos
    func procesar(){
        var salida = "Procesador \"\(nombre)\" | Cantidad de procesos \(procesos.count)\n"
        while !procesos.isEmpty{
            let proceso = procesos.removeFirst()
            salida += "-----> Corriendo proceso \"\(proceso.nombre)\"\n"
            if let error = proceso.procesar(){
                salida += "HA OCURRIDO UN ERROR: \(error)\n"
                procesos = []
                break
            }
            else if let mensaje = proceso.mostrar?(){
                salida += "\(mensaje)\n"
            }
        }
        salida += "Fin de la rutina"
        print(salida)
    }


}

// MARK: Procesos

class SumaLista: Proceso{

    let nombre: String = "Sumador de lista de enteros"

    let lista: [Int]
    var resultado: Int = 0

    init(entrada: [Int]){
        self.lista = entrada
    }

    func procesar() -> String? {
        for i in lista{
            resultado += i
        }
        return nil
    }

    func mostrar() -> String {
        return "El resultado de sumar los elementos de \(lista) es \(resultado)"
    }

}

class Divide: Proceso{

    let nombre: String = "Divisor de numeros"

    var x,y: Double
    var resultado: Double = 0

    init(x: Double, y: Double){
        self.x = x
        self.y = y
    }

    func procesar() -> String? {
        if y == 0{
            return "No se puede dividir por cero"
        }
        resultado = x / y
        return nil
    }

    func mostrar() -> String {
        return "El resultado de dividir \(x) por \(y) es \(resultado)"
    }

}

class Nada: Proceso {

    let nombre: String = "Proceso nulo"

    func procesar() -> String? {
        return nil
    }

}

// MARK: prueba

let procesador = Procesador(nombre: "Mi Equipo")
procesador.agregar(proceso: SumaLista(entrada: [1,2,3,4]))
procesador.agregar(proceso: Nada())
procesador.agregar(proceso: Divide(x: 5, y: 0))
procesador.procesar()
