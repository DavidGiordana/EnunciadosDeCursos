# Protocolos PT 6

* Crear un protocolo Proceso que tenga:
    * Propiedad nombre.
    * Método opcional llamado mostrar que no tome nada y retorne un String.
    * Método llamado procesar que no tome nada y retorne un String.
* Definir una clase Procesador que tenga:
    * Lista de procesos.
    * Nombre.
    * Inicializador.
    * Método para agregar procesos.
    * Método porcesar.
Este último debe mostar información del procesador y respetar la siguente estructura de ejecucióm
* LLamar a la función procesar de un proceso. Si este retorna nil significa que su ejecución fue satisfactoria. En caso contrario hubo algún problema (no es una excepción necesariamente, es una forma de representarlo) y el String resultante es una descripción del error.    
* Si el paso anterior no falló mostrar el mensaje de salida del programa en pantalla en caso de existir alguno.
El funcionamiento de la lista debe emular a una cola, es decir, que el primero que entró será el primero atendido y será eliminado de la lista. Al finalizar la ejecución del método procesar la lista de procesos debe quedar vacía.

* Crear procesos propios, al menos 3 y hacer pruebas con el procesador.
