import Foundation

/*
# Enumeraciones

* Crear una enumeracion llamada Vocal que tenga los siguentes elementos:
   * vocalMinuscula que a su vez almacene un caracter.
   * vocalMayuscula que a su vez guarde un caracter.
   * otro, este caso no debe almacenar nada.
* Crear una funcion que tome un String y retorne una lista de Vocal. Esta debe convertir los caracteres de la cadena de entrada en Vocal. Las asignaciones son claras con los meros nombres (es decir, vocalMinuscula tiene que almacenar una vocal minúscula y asi sucesivamente).
* Crear dos funciones que tomen una lista de Vocal y no retornen nada. Estas recivitrán el nombre de imprimirMinusculas e imprimirMayusculas. Las funciones deben imprimir una cadena que contenga solo elementos segun el nombre de su función.
*/

enum Vocal {
    case vocalMinuscula(Character)
    case vocalMayuscula(Character)
    case noVocal
}

func conversor(_ cadena: String)->[Vocal]{
    var lista = [Vocal]()
    for caracter in cadena.characters{
        switch caracter {
            case "a", "e", "i", "o" ,"u": lista.append(.vocalMinuscula(caracter))
            case "A", "E", "I", "O" ,"U": lista.append(.vocalMayuscula(caracter))
            default: lista.append(.noVocal)
        }
    }
    return lista
}

func imprimirMinusculas(_ lista: [Vocal]){
    var cadena = ""
    for vocal in lista{
        switch vocal{
            case .vocalMinuscula(let x): cadena.append(x)
            default: break
        }
    }
    print(cadena)
}

func imprimirMayusculas(_ lista: [Vocal]){
    var cadena = ""
    for vocal in lista{
        switch vocal{
            case .vocalMayuscula(let x): cadena.append(x)
            default: break
        }
    }
    print(cadena)
}

let cadenaPrincipal = "hola mundo - HOLA MUNDO"
let vocales: [Vocal] = conversor(cadenaPrincipal)
imprimirMinusculas(vocales)
imprimirMayusculas(vocales)
