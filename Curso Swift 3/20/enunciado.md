# Enumeraciones

* Crear una enumeracion llamada Vocal que tenga los siguentes elementos:
   * vocalMinuscula que a su vez almacene un caracter.
   * vocalMayuscula que a su vez guarde un caracter.
   * otro, este caso no debe almacenar nada.
* Crear una funcion que tome un String y retorne una lista de Vocal. Esta debe convertir los caracteres de la cadena de entrada en Vocal. Las asignaciones son claras con los meros nombres (es decir, vocalMinuscula tiene que almacenar una vocal minúscula y asi sucesivamente).
* Crear dos funciones que tomen una lista de Vocal y no retornen nada. Estas recivitrán el nombre de imprimirMinusculas e imprimirMayusculas. Las funciones deben imprimir una cadena que contenga solo elementos segun el nombre de su función.

