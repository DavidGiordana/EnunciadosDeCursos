import Foundation

/*
# Strings PT 2

* Crear una variable de tipo String con la cadena ABCD. Imprimir los tres primeros caracteres utlilizando indices.

* Comprobar si el String "AB" es prefijo y si el String "C#" es sufijo de cadena.

* Eliminar el último caracter de cadena.
*/

var cadena: String = "ABCD"
print(cadena[cadena.startIndex])
print(cadena[cadena.index(after: cadena.startIndex)])
print(cadena[cadena.index(cadena.startIndex, offsetBy: 2)])

print("Al comprobar si \"AB\" es prefijo de cadena obtenemos \(cadena.hasPrefix("AB"))")
print("Al comprobar si \"C#\" es sufijo de cadena obtenemos \(cadena.hasSuffix("C#"))")

cadena.remove(at: cadena.index(before: cadena.endIndex))
print(cadena)
