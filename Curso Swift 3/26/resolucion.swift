import Foundation

/*
# Inicializadores

Para esta ocasioón como la idea es practicar el uso de inicializadores tomar laa estructuraa Rango y RGB vistas en tutoriales anteriores e incorporarle inicializadores de manera tal que no cuenten con valores por defecto delegando esta configuración al desarrollador que las utilice.
*/

struct Rango {

    var inicio: Int
    var fin: Int

    var mitad: Int {
        return inicio + (fin - inicio) / 2
    }

    init(inicio: Int, fin: Int){
        self.inicio = inicio
        self.fin = fin
    }

}


struct RGB {

    var r: Int
    var g: Int
    var b: Int

    init(r: Int, g: Int, b: Int){
        self.r = r
        self.g = g
        self.b = b
    }
}


let rango: Rango = Rango(inicio: 10, fin: 26)
print("El centro del rango que comienza en \(rango.inicio) y termina en \(rango.fin) es \(rango.mitad)")

let color: RGB = RGB(r: 10, g: 44, b: 224)
print("La constante color representa al color (R: \(color.r), G: \(color.g), B: \(color.b)) en RGB")
