# Funciones PT 2

* Crear una lista de enteros que contenga al menos 5 numeros.
* Crear una funcion llamada valorAbsolutoLista que toma una lista de enteros y no retorna nada. Esta funcion debe modificar la lista que se toma como entrada para almacenar en cada lugar el valor absoluto del numero original. Para calcular el valor absoluto crear una funcion dentro de la misma funcion que toma un entero entero y retorna un entero que calcule el valor absoluto (Recordar ejercicios pasados).
