import Foundation

/*
# Condicional if

* Crear un pequeño programa que dado los valores almacenados en dos variables de tipo entero llamadas a y b e imprima un mensaje indicando si a es menor que b, a es igual a b o a es mayor que b.
*/

let valor1: Int = 10
let valor2: Int = 7
if valor1 < valor2 {
    print("El valor \(valor1) es menor que el valor \(valor2)")
}
else if valor1 == valor2 {
    print("El valor \(valor1) es igual al valor \(valor2)")
}
else{
    print("El valor \(valor1) es mayor que el valor \(valor2)")
}
