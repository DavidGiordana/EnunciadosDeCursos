import Foundation

/**
# Genericos PT 2 y 3

Crear un protocolo llamado asocaición que contenga
* Tipos asociados K y V
* subscript que tome un elemento de tipo K y retorne uno de tipo V (debe permitir escritura)
* Propiedades de solo lectura isEmpty y count
* Propiedad de solo lectura claves de tipo lista de K
Crear una estrucutura Diccionario (Será una nueva implementación genérica) que se adapte a Asociación.
Considerar que la semántica debe ser la misma de un diccionario, tomando dos tipos genéricos del cual el primero debe ajustarse al protocolo Equatable (que se puede igualar).
La propiedad claves retorna una lista con las claves del diccionario.

Crear las funciones:
* imprimirDiccionario que tome un diccionario e imprima su contenido
* clavesOrdenadas que tome un tipo genérico que se adapte a Asociacion y su tipo asociado K (correspondiente a la llave del diccionario) sea Comparable. Esta función debe evalar si las claves del diccionario están ordenados.

**/


protocol Asociacion {

    associatedtype K
    associatedtype V

    subscript (clave: K)-> V {get set}

    var isEmoty: Bool {get}
    var count: Int {get}

    var claves: [K] {get}

}

struct Diccionario<Clave: Equatable, Valor>: Asociacion {

    var items = [(clave: Clave, valor: Valor)]()

    subscript (clave: Clave)->Valor? {
        get {
            for (k,v) in items{
                if k == clave{
                    return v
                }
            }
            return nil
        }
        set(nuevoValor){
            for i in 0..<items.count {
                if items[i].clave == clave {
                    if let nv = nuevoValor {
                        items[i] = (clave, nv)
                    }
                    else{
                        items.remove(at: i)
                    }
                    return
                }
            }
            if let nv = nuevoValor {
                items.append((clave, nv))
            }
        }
    }

    var isEmoty: Bool { return items.isEmpty }
    var count: Int { return items.count }

    var claves: [Clave] {
        return items.map {$0.0}
    }

}


func imprimirDiccionario(_ diccionario: Diccionario<Int, String>){
    for clave in diccionario.claves{
        print("\(clave): \(diccionario[clave]!)")
    }
}

func clavesOrdenadas<T: Asociacion>(_ asoc: T)->Bool where T.K: Comparable{
    let claves = asoc.claves
    if claves.isEmpty {
        return true
    }
    var temp = claves[0]
    for i in 1..<claves.count{
        let elemento = claves[i]
        if temp > elemento {
            return false
        }
        temp = elemento
    }
    return true
}

var diccionario = Diccionario<Int,String>()
diccionario[435] = "Carlos"
diccionario[111] = "Ruben"
diccionario[5] = "Ana"
imprimirDiccionario(diccionario)
print("")
diccionario[111] = nil
imprimirDiccionario(diccionario)
print("")
let ordenadas = clavesOrdenadas(diccionario) ? "" : "no "
print("Las claves de la asociación \(ordenadas)están ordenadas")
