# Genericos PT 2 y 3

Crear un protocolo llamado asocaición que contenga
* Tipos asociados K y V
* subscript que tome un elemento de tipo K y retorne uno de tipo V (debe permitir escritura)
* Propiedades de solo lectura isEmpty y count
* Propiedad de solo lectura claves de tipo lista de K
Crear una estrucutura Diccionario (Será una nueva implementación genérica) que se adapte a Asociación.
Considerar que la semántica debe ser la misma de un diccionario, tomando dos tipos genéricos del cual el primero debe ajustarse al protocolo Equatable (que se puede igualar).
La propiedad claves retorna una lista con las claves del diccionario.

Crear las funciones:
* imprimirDiccionario que tome un diccionario e imprima su contenido
* clavesOrdenadas que tome un tipo genérico que se adapte a Asociacion y su tipo asociado K (correspondiente a la llave del diccionario) sea Comparable. Esta función debe evalar si las claves del diccionario están ordenados.
