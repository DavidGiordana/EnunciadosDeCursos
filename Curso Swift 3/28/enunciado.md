# Inicializadres PT 3

* Crear una clase llamada ISP (Internet Service Provider o proveedor de servicio de internet) la cual debe tener una constante de tipo (static) de tipo entero llamada anchoDeBandaTotal y una varaible de clase entera llamada anchoDeBandaDisponible la cual en principio debe ser igual a AnchoDeBandaTotal. Además es necesario incorporar dos métodos de tipo:
   * solicitarAnchoDeBanda que tome un entero y retorna otro. Intenta restar del ancho de banda disponible la cantidad solicitada y retorna el mismo numero. Si la solicitud es más grande de lo que se dispone el contador de ancho de banda disponible será sero y se retornará la cantidad que había disponible.
   * devolverAnchoDeBanda que toma un entero y no retorna nada. Esta funcion simplemente reincorpora cierta cantidad de ancho de banda.

* Crear una clase cliente que tenga las propiedades nombre y anchoDeBandaEnUso y los métodos:
   * aumentarAnchoDeBanda y disminuirAnchoDebanda que tomen un entero e intenten incrementar el ancho de banda en uso.
   * darDeBaja que retorne todo el acho de banda al proveedor.
Para que tenga más sentido el ejercicio esta clase debe tener un inicializador que tome un nombre que no puede estar vacio (en dicho caso debe fallar) y un abcho de banda base.
