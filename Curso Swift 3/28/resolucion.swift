import Foundation

/*
# Inicializadres PT 3

* Crear una clase llamada ISP (Internet Service Provider o proveedor de servicio de internet) la cual debe tener una constante de tipo (static) de tipo entero llamada anchoDeBandaTotal y una varaible de clase entera llamada anchoDeBandaDisponible la cual en principio debe ser igual a AnchoDeBandaTotal. Además es necesario incorporar dos métodos de tipo:
   * solicitarAnchoDeBanda que tome un entero y retorna otro. Intenta restar del ancho de banda disponible la cantidad solicitada y retorna el mismo numero. Si la solicitud es más grande de lo que se dispone el contador de ancho de banda disponible será sero y se retornará la cantidad que había disponible.
   * devolverAnchoDeBanda que toma un entero y no retorna nada. Esta funcion simplemente reincorpora cierta cantidad de ancho de banda.

* Crear una clase cliente que tenga las propiedades nombre y anchoDeBandaEnUso y los métodos:
   * aumentarAnchoDeBanda y disminuirAnchoDebanda que tomen un entero e intenten incrementar el ancho de banda en uso.
   * darDeBaja que retorne todo el acho de banda al proveedor.
Para que tenga más sentido el ejercicio esta clase debe tener un inicializador que tome un nombre que no puede estar vacio (en dicho caso debe fallar) y un abcho de banda base.
*/

class ISP {

    static let anchoDeBandaTotal = 100
    static var anchoDeBandaDisponible = anchoDeBandaTotal

    static func solicitarAnchoDeBanda(cantidad: Int)->Int{
        if cantidad > anchoDeBandaDisponible{
            let temp = anchoDeBandaDisponible
            anchoDeBandaDisponible = 0
            return temp
        }
        anchoDeBandaDisponible -= cantidad
        return cantidad
    }

    static func devolverAnchoDeBanda(cantidad: Int){
        anchoDeBandaDisponible += cantidad
    }

}

class Cliente {

    var nombre: String

    var anchoDeBandaEnUso: Int

    init?(nombre: String, anchoDeBandaInicial: Int){
        if nombre.isEmpty {
            return nil
        }
        if anchoDeBandaInicial <= 0 {
            return nil
        }
        self.nombre = nombre
        self.anchoDeBandaEnUso = ISP.solicitarAnchoDeBanda(cantidad: anchoDeBandaInicial)
    }

    convenience init?(nombre: String){
        self.init(nombre: nombre, anchoDeBandaInicial: 1)
    }

    func aumentarAnchoDeBanda(cantidad: Int){
        self.anchoDeBandaEnUso += ISP.solicitarAnchoDeBanda(cantidad: cantidad)
    }

    func darDeBaja(){
        ISP.devolverAnchoDeBanda(cantidad: self.anchoDeBandaEnUso)
        self.anchoDeBandaEnUso = 0
    }

}

let cliente1 = Cliente(nombre: "", anchoDeBandaInicial: 14)
let cliente2 = Cliente(nombre: "Juan", anchoDeBandaInicial: 50)
let cliente3 = Cliente(nombre: "Ruben")
let clientes = [cliente1, cliente2, cliente3]

func imprimirInformación(){
    for cliente in clientes{
        if let cli = cliente{
            print("El cliente \(cli.nombre) tiene un ancho de banda asignado de \(cli.anchoDeBandaEnUso)")
        }
    }
    print("El proveedor de internet tiene un ancho de banda disponible de \(ISP.anchoDeBandaDisponible)\n")
}

imprimirInformación()
cliente3?.aumentarAnchoDeBanda(cantidad: 50)
imprimirInformación()
cliente2?.darDeBaja()
imprimirInformación()
