import Foundation

/*
# Extensiones

Hacer una extensión del tipo Double que incluya:
* promiedad mitad
* propiedad doble
* propiedad cubo (aplica el cubo a un numero)
* propiedad parteEntera (Numero entero que no contiene los decimales del numero original)
* inicializador que toma dos listas de enteros. Cada una representará la parte entera y la parte decimal de un numero en punto flotante respectivamente. Este debe construir un numero Double
* método getDigits que retorne una tupla con dos listas de enteros representando la parte entera y decimal del numero
* subscript que tome un entero y retorne un entero. Este debe retornar en n-esimo dígito de la parte entera de derecha a izquierda si el numero es mayor o igual a cero (similar a lo hecho en el ejemplo de los videos). En caso de ser negativo el conteo se hará de izquieda a derecha siendo -1 el primer d+igito después del punto.
Una pruba del inicializador y del método getDigits es componerlos. Si todo salió bien el resultado debería coincidir con el número original.
*/

extension Double {

    var mitad: Double { return self / 2 }

    var doble: Double { return self * 2 }

    var cubo: Double { return self * self * self }

    var parteEntera: Int { return Int(self) }

    func getDigits()->(parteEntera: [Int], parteDecimal: [Int]){
        let parts = String(self).components(separatedBy: ".")
        var parteEntera = [Int]()
        var parteDecimal = [Int]()
        for char in parts[0].characters{
            parteEntera.append(Int(String(char))!)
        }
        for char in parts[1].characters{
            parteDecimal.append(Int(String(char))!)
        }
        return (parteEntera, parteDecimal)
    }

    init?(parteEntera: [Int], parteDecimal: [Int]){
        var entera = ""
        var decimal = ""
        for i in parteEntera{
            entera.append("\(i)")
        }
        for i in parteDecimal{
            decimal.append("\(i)")
        }
        self.init("\(entera).\(decimal)")
    }

    subscript(index: Int) ->  Int {
        get{
            var i = index
            var temp: Double = 1
            if index < 0 {
                i = -index
            }
            for _ in 0..<i{
                temp *= 10
            }
            let entero = Int(index < 0 ? self * temp : self / temp)
            return entero % 10
        }
    }

}

let lista = [1.7, 0.002, 4.9]
let numero = 3.14
for item in lista{
    let (pe, pd) = item.getDigits()
    print(Double(parteEntera: pe, parteDecimal: pd)!)
}

print("")

print("La parte entera de \(numero) es \(numero.parteEntera)")
print("la mitad de \(numero) es \(numero.mitad)")
print("El doble de \(numero) es \(numero.doble)")
print("El cubo de \(numero) es \(numero.cubo)")
