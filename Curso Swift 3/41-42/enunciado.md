# Extensiones

Hacer una extensión del tipo Double que incluya:
* promiedad mitad
* propiedad doble
* propiedad cubo (aplica el cubo a un numero)
* propiedad parteEntera (Numero entero que no contiene los decimales del numero original)
* inicializador que toma dos listas de enteros. Cada una representará la parte entera y la parte decimal de un numero en punto flotante respectivamente. Este debe construir un numero Double
* método getDigits que retorne una tupla con dos listas de enteros representando la parte entera y decimal del numero
* subscript que tome un entero y retorne un entero. Este debe retornar en n-esimo dígito de la parte entera de derecha a izquierda si el numero es mayor o igual a cero (similar a lo hecho en el ejemplo de los videos). En caso de ser negativo el conteo se hará de izquieda a derecha siendo -1 el primer d+igito después del punto.
Una pruba del inicializador y del método getDigits es componerlos. Si todo salió bien el resultado debería coincidir con el número original.
