# Conteo automático de referencias PT 2

* Crear una clase llamada Autenticador la cual cuente con:
   * Propiedad almacenada opcional usuarioActual.
   * Propiedad almacenada de tipo [String: int] el cual contendrá una asociación de usuario contraseña.
   * Propiedad almacenada que contenga una clausura que no tome nada y restorne un string. Esta debe generar un mensaje de bienvenida al usuario actual y debe llamarse saludar.
   * Método iniciarSesion que tome un usuario
   * Método iniciarSesion que tome un usuario y contraseña, compruebe si se pued iniciar sesion. En caso afirmativo asignar el usuario a usuarioActual y llamar a la clausura saludar.
   * Método cerrarSesion que no tome nada (hace lo que dice)
tanto el inicializador como destructor deben imprimir un mensaje cuando son llamados. Para evitar trabajo extra se le permitira al desarrollador que la lista de usuarios registrados contenga valores establecidos por defecto (se peude hardcodear).

* Crear una insntancia de Autenticador, guardarle en una propeidad opcional, utilizarla y finalmente asignar nil.
* Esta implementación genera un ciclo de referencias fuertes. Solucionarlo.
