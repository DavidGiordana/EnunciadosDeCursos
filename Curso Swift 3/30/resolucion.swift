import Foundation

/*
# Conteo automático de referencias PT 2

* Crear una clase llamada Autenticador la cual cuente con:
   * Propiedad almacenada opcional usuarioActual.
   * Propiedad almacenada de tipo [String: int] el cual contendrá una asociación de usuario contraseña.
   * Propiedad almacenada que contenga una clausura que no tome nada y restorne un string. Esta debe generar un mensaje de bienvenida al usuario actual y debe llamarse saludar.
   * Método iniciarSesion que tome un usuario
   * Método iniciarSesion que tome un usuario y contraseña, compruebe si se pued iniciar sesion. En caso afirmativo asignar el usuario a usuarioActual y llamar a la clausura saludar.
   * Método cerrarSesion que no tome nada (hace lo que dice)
tanto el inicializador como destructor deben imprimir un mensaje cuando son llamados. Para evitar trabajo extra se le permitira al desarrollador que la lista de usuarios registrados contenga valores establecidos por defecto (se peude hardcodear).

* Crear una insntancia de Autenticador, guardarle en una propeidad opcional, utilizarla y finalmente asignar nil.
* Esta implementación genera un ciclo de referencias fuertes. Solucionarlo.
*/

class Autenticador {

    var usuarioActual: String?

    var usuariosRegistrados: [String: Int]

    lazy var saludar: ()->String = { [unowned self] in
        if let nombre = self.usuarioActual{
            return "Bienvenido \(nombre)"
        }
        return ""
    }

    init(){
        usuarioActual = ""
        usuariosRegistrados = ["Carlos": 123, "Ruben": 444]
    }

    func iniciarSesion(user: String, pass: Int){
        if let contra = usuariosRegistrados[user]{
            if contra == pass{
                usuarioActual = user
                print(saludar())
                return
            }
        }
        print("Error: La información ingresada es incorrecta")
    }

    func cerrarSesion(){
        usuarioActual = ""
    }

    deinit {
        print("Se está destruyendo el Autenticador")
    }

}

var aut: Autenticador? = Autenticador()
if let a = aut{
    a.iniciarSesion(user: "Carlos", pass: 234)
    a.iniciarSesion(user: "Carlos", pass: 123)
    a.cerrarSesion()
    a.iniciarSesion(user: "Ruben", pass: 444)
}
aut = nil
