import Foundation

/**
# Protocolos PT 4

Basandose en la version de diccionario modificada en el episodio 35:
* Crear un protocolo llamado Descriptible que tenga una propiedad de solo lectura de tipo String llamada descripción.
* Extender el diccionario para que se adapte al protocolo. La descripción debe mostrar todos los elementos del diccionario.
**/
protocol Asociacion {

    var isEmpty: Bool {get}
    var count: Int {get}

    func agregar(clave: String, valor: Int?)

    func eliminar(clave: String)->Int?

    func existe(clave: String)->Bool

    func obtenerValor(clave: String)->Int?

}

class Diccinario: Asociacion {

    var datos: [(String, Int)] = [(String, Int)]()

    func limpiar(){
        self.datos = []
    }

    var count: Int {
        return datos.count
    }

    var isEmpty: Bool {
        return datos.isEmpty
    }

    func agregar(clave: String, valor: Int?){
        var agregado: Bool = false
        if !isEmpty {
            for i in 0..<count{
                let (k,_): (String, Int) = datos[i]
                if clave == k{
                    if let nv = valor{
                        datos[i] = (k, nv)
                    }
                    else{
                        datos.remove(at: i)
                    }
                    agregado = true
                    break
                }
            }
        }
        if !agregado, let nv = valor{
            datos.append((clave, nv))
        }
    }

    func eliminar(clave: String)->Int?{
        if !isEmpty {
            for i in 0..<count{
                let (k,v): (String, Int) = datos[i]
                if clave == k{
                    datos.remove(at: i)
                    return v
                }
            }
        }
        return nil
    }

    func existe(clave: String)->Bool{
        return obtenerValor(clave: clave) != nil
    }

    func obtenerValor(clave: String)->Int?{
        var valor: Int?
        for (k,v) in datos{
            if (k == clave){
                valor = v
                break
            }
        }
        return valor
    }


    subscript(llave: String) ->  Int? {
        get{
            return obtenerValor(clave: llave)
        }
        set{
            agregar(clave: llave, valor: newValue)
        }
    }

}

protocol Descriptible {

    var descripcion: String {get}

}

extension Diccinario: Descriptible{

    var descripcion: String{
        var ret = "["
        for (clave, valor) in datos{
            ret += " \"\(clave)\": \(valor),"
        }
        ret.remove(at: ret.index(before: ret.endIndex))
        return ret + " ]"
    }

}

var dict = Diccinario()
dict["Valor 1"] = 123
dict["Valor 2"] = 456
dict["Valor 3"] = 789
print(dict.descripcion)
