# Protocolos PT 4

Basandose en la version de diccionario modificada en el episodio 35:
* Crear un protocolo llamado Descriptible que tenga una propiedad de solo lectura de tipo String llamada descripción.
* Extender el diccionario para que se adapte al protocolo. La descripción debe mostrar todos los elementos del diccionario.
