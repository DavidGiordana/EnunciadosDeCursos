import Foundation

/*
# Propirdades

* Crear una estructura llamada Rango la cual debe tener dos propiedades variables de tipo entero que tengan como valor por defecto cero. Estas recibiran los nombres inicio y fin para representar el comienzo y final del rango. Además tiene que contar con una propiedad computada llamada mitad que calcule la mitad entre los principio y el final del rango.
* Definir una clase llamada Area que debe contener dos propiedades almacenadas de tipo rango llamadas largo y ancho respectivamente y una propiedad computada llamada centro de tipo tupla de enteros la cual calcule el puto medio entre el ancho y el largo en ese orden.
* Crear una instancia de Area, midificar sus valores e imprimir el punto central.
*/

struct Rango {

    var inicio: Int = 0, fin = 0

    var mitad: Int {
        return inicio + (fin - inicio) / 2
    }
}

class Area {
    var largo: Rango = Rango()
    var ancho: Rango = Rango()

    var centro: (Int, Int) {
        return (largo.mitad, ancho.mitad)
    }

}

let area: Area = Area()
area.largo.fin = 20
area.ancho.inicio = 3
area.ancho.fin = 17


let centro = area.centro
print("El punto central del area es \(centro)")
