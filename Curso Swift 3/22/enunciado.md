# Propirdades

* Crear una estructura llamada Rango la cual debe tener dos propiedades variables de tipo entero que tengan como valor por defecto cero. Estas recibiran los nombres inicio y fin para representar el comienzo y final del rango. Además tiene que contar con una propiedad computada llamada mitad que calcule la mitad entre los principio y el final del rango.
* Definir una clase llamada Area que debe contener dos propiedades almacenadas de tipo rango llamadas largo y ancho respectivamente y una propiedad computada llamada centro de tipo tupla de enteros la cual calcule el puto medio entre el ancho y el largo en ese orden.
* Crear una instancia de Area, midificar sus valores e imprimir el punto central.
