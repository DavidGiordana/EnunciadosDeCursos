import Foundation

/*
# Bucles while y repeat-while

* Crear tres variables de tipo entero llamadas valor1, valor2 y salida. En valor1 y valor2 tienen que contener dos valores positivos. En salida hay que alamcenar en resultado de multiplicar valor1 y valor2 sin emplear el operador de multiplicación, En este caso utilizar un bucle while.
* Crear una variante del programa anterior que utilice un bucle do-while.
* Para simplificar el trabajo supondremos que valor1 y valor2 no pueden ser negativos.
*/

var valor1: Int = 3
var valor2: Int = 2
var resultado: Int = 0
var contador = 0;

while contador < valor1{
    resultado += valor2
    contador += 1
}
print("\(valor1)*\(valor2) = \(resultado)")

// Limpieza de variables de trabajo
contador = 0
resultado = 0

repeat {
    resultado += valor2
    contador += 1
} while contador < valor1
resultado = valor1 == 0 ? 0 : resultado
print("\(valor1)*\(valor2) = \(resultado)")
