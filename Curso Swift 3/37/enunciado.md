# Protocolos PT 3

Crear:
* Protocolo Archivo que contenga:
   * propiedad de lectura y escritura llamada nombre de tipo String
   * propiedad de lectura y escritura llamada contenido de tipo String
   * propiedad de lectura y escritura llamada tamaño de tipo entero
* Ptotocolo ArchivoDelegate que contenga
   * método que tome un archivo llamado describir
   * método que tome un archivo y un String con la etiqueta externa nuevoContenido e interna contenido
* Clase ArchivoDeTexto se adapte al protocolo Archivo
   * Al crear el arhivo este debe estar vacío.
   * El tamaño se calcula en base a la cantidad de caracteres del contenido (Supondremos que esta represetación se hace en bytes respetando el tamaño de un caracter en ascii, es decir 1 caracter = 1 byte)
   * Debe existir una propiedad de tipo ArchivoDelegate opcional para almacenar el delegado. En los métodos debe considerarse el llamar a e los delegados también.
   * El funcionamiento de los métodos queda a interpresación del alumno
* Crear una clase Administrador1 que se adapte al protocolo ArchivoDelegate
   * Los métodos quedan a libre interpretación del alumno
