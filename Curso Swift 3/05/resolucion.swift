import Foundation

/*
# Operadores básicos PT 1

* Crear una cosntnte de tipo String llamada cadena1 con el texto "Este es " y una segunda constante del mismo tipo llamada cadena2 con el texto "un String". imprimir el resultado de concatenar ambas cadenas.
* Crear dos constantes de tipo entero llamadas valor1 y valor2. Asignar dos valores cualesquiera e imprima el resultado de realizar las siguentes operaciones: suma, resta, multiplicación, division, modulo, opuesto.
*/

let cadena1: String = "Este es "
let cadena2: String = "un String"
print(cadena1 + cadena2)

let valor1: Int = 17
let valor2: Int = 3
print(valor1 + valor2)
print(valor1 - valor2)
print(valor1 * valor2)
print(valor1 / valor2)
print(valor1 % valor2)
print(-valor1)
print(-valor2)
